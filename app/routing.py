from authlib.integrations.flask_oauth2 import current_token
from flask_login import current_user
from werkzeug.exceptions import Forbidden
from werkzeug.routing import IntegerConverter, PathConverter

from app.decorators import require_oauth
from app.repository import model_service
from app.roles import Roles


class UserSelfConverter(IntegerConverter):
    regex = r"(\d+|self)"

    @require_oauth(optional=True)
    def to_python(self, value):
        from app.service import user_service
        from app.service import role_service

        if value == "self":
            if current_token:
                return current_token.user
            if not current_user.is_anonymous:
                return current_user

        # Tokens prioritize sessions.
        user = current_user
        if current_token:
            user = current_token.user

        if not role_service.user_has_role(user, Roles.USER_WRITE):
            raise Forbidden()

        user_id = super().to_python(value)
        return user_service.get_user_by_id(user_id)

    @require_oauth(optional=True)
    def to_url(self, value):
        if current_user == value or value == "self":
            return "self"

        if current_token and current_token.user == value:
            return "self"

        return super().to_url(value.id)


class NavigationEntryConverter(PathConverter):
    def to_python(self, value):
        from app.service import navigation_service

        path = super().to_python(value)
        return navigation_service.get_entry_by_id(path)

    def to_url(self, value):
        return super().to_url(value.id)


class PageConverter(IntegerConverter):
    """Converts a page_id to a page.

    This also returns pages that have been soft-deleted, unlike the general
    model converter.

    This converter is not used for the user-facing front-end. That uses
    `PagePathConverter`."""

    def to_python(self, value):
        from app.service import page_service

        page_id = super().to_python(value)
        return page_service.get_page_by_id(page_id, incl_deleted=True)

    def to_url(self, value):
        return super().to_url(value.id)


def create(model_cls):
    class ModelConverter(IntegerConverter):
        def to_python(self, value):
            object_id = super().to_python(value)
            return model_service.get_by_id(model_cls, object_id)

        def to_url(self, value):
            return super().to_url(value.id)

    return ModelConverter
