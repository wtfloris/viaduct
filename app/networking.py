import logging
from requests.adapters import HTTPAdapter
from requests.models import Response
from requests.exceptions import RequestException
from app.exceptions.base import ServiceUnavailableException


_logger = logging.getLogger(__name__)


class ServiceOfflineResponse(Response):
    def __init__(self, service_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.service_name = service_name
        self.status_code = 503

    @property
    def content(self):
        raise ServiceUnavailableException(
            f"External service '{self.service_name}' is currently unavailable."
            " This may result in some features on this page being unavailable."
        )


class DefaultResponseAdapter(HTTPAdapter):
    response = ServiceOfflineResponse

    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name

    def send(
        self, request, stream=False, timeout=None, verify=True, cert=None, proxies=None
    ) -> Response:
        try:
            response = super().send(request, stream, timeout, verify, cert, proxies)
            # Bad request, Not Found, etc. may be expected status codes.
            if response.status_code >= 500:
                response.raise_for_status()
            return response

        except RequestException as e:
            _logger.warning(e)
            return ServiceOfflineResponse(self.name)
