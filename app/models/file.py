from sqlalchemy import Column, Enum, String, UniqueConstraint

from app.enums import FileCategory
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class File(BaseEntity):
    """Contains the metadata of an uploaded file."""

    __tablename__ = "file"

    hash: str = Column(String(200), nullable=False)
    extension = Column(String(20), nullable=False)

    category = Column(Enum(FileCategory, name="file_category"), nullable=False)
    display_name = Column(String(200))

    @property
    def full_display_name(self):
        if not self.display_name:
            return None

        name = self.display_name
        if self.extension:
            name += "." + self.extension

        return name

    @property
    def require_membership(self):
        return self.category in {
            FileCategory.UPLOADS_MEMBER,
            FileCategory.ALV_DOCUMENT,
            FileCategory.EXAMINATION,
        }

    __table_args__ = (UniqueConstraint("display_name", "extension"),)
