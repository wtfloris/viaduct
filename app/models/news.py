import datetime
from datetime import date
from typing import TYPE_CHECKING

from flask_login import current_user
from sqlalchemy import Boolean, Column, Date, Integer, String, Text, event
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.user import User  # noqa


@mapper_registry.mapped
class News(BaseEntity):
    __tablename__ = "news"
    nl_title = Column(String(256))
    en_title = Column(String(256))
    nl_content = Column(Text)
    en_content = Column(Text)

    user_id = Column(Integer, ForeignKey("user.id"))
    user: "User" = relationship("User", backref=backref("news", lazy="dynamic"))
    needs_paid = Column(Boolean, default=False, nullable=False)

    publish_date = Column(Date)

    # TODO remove and fix usages.
    def __init__(
        self,
        nl_title="",
        nl_content="",
        en_title="",
        en_content="",
        user_id=None,
        publish_date=None,
        needs_paid=False,
    ):

        self.title = ""
        self.content = ""
        self.nl_title = nl_title
        self.nl_content = nl_content
        self.en_title = en_title
        self.en_content = en_content

        self.user_id = user_id
        self.needs_paid = needs_paid
        if publish_date:
            self.publish_date = publish_date
        else:
            self.publish_date = date.today()

    def __str__(self):
        return "{} ({})".format(self.title, self.publish_date)

    @property
    def modified_after_creation(self):
        return (self.modified - self.created) >= datetime.timedelta(seconds=1)

    def can_read(self, user=current_user):
        return not self.needs_paid or user.has_paid

    def get_localized_title_content(self, locale=None):
        if not locale:
            locale = get_locale()

        nl_available = self.nl_title and self.nl_content
        en_available = self.en_title and self.en_content
        if locale == "nl" and nl_available:
            title = self.nl_title
            content = self.nl_content
        elif locale == "en" and en_available:
            title = self.en_title
            content = self.en_content
        elif nl_available:
            title = self.nl_title + " (Dutch)"
            content = self.nl_content
        elif en_available:
            title = self.en_title + " (Engels)"
            content = self.en_content
        else:
            title = "N/A"
            content = "N/A"

        return title, content


@event.listens_for(News, "load")
def set_news_locale(news, _):
    """
    Fill model content according to language.

    This function is called after an News model is filled with data from
    the database, but before is used in all other code.

    Use the locale of the current user/client to determine which language to
    display on the whole website. If the users locale is unavailable, select
    the alternative language, suffixing the title of the news with the
    displayed language.
    """

    news.title, news.content = news.get_localized_title_content()
