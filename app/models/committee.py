from typing import TYPE_CHECKING

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Text, event
from sqlalchemy.orm import relationship
from sqlalchemy.util import deprecated

from app import get_locale
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.group import Group  # noqa
    from app.models.user import User  # noqa
    from app.models.page import Page  # noqa


@mapper_registry.mapped
class Committee(BaseEntity):
    __tablename__ = "committee"

    nl_name = Column(String(256), nullable=False)
    en_name = Column(String(256), nullable=False)

    page_id = Column(Integer, ForeignKey("page.id"), nullable=False)

    group_id = Column(Integer, ForeignKey("group.id"), nullable=False)
    coordinator_id = Column(Integer, ForeignKey("user.id"))
    picture_file_id = Column(Integer, ForeignKey("file.id"))
    coordinator_interim = Column(Boolean, nullable=False, default=False)
    open_new_members = Column(Boolean, nullable=False, default=False)
    en_tags = Column(String(256), nullable=False, default="")
    nl_tags = Column(String(256), nullable=False, default="")
    pressure = Column(Integer, nullable=False, default=0)

    group: "Group" = relationship("Group")
    coordinator: "User" = relationship("User")
    page: "Page" = relationship("Page")

    def get_localized_name(self, locale=None):
        if not locale:
            locale = get_locale()

        if locale == "nl" and self.nl_name:
            return self.nl_name
        elif locale == "en" and self.en_name:
            return self.en_name
        elif self.nl_name:
            return self.nl_name + " (Dutch)"
        elif self.en_name:
            return self.en_name + " (Engels)"
        else:
            return "N/A"

    def get_localized_tags(self, locale=None):
        if not locale:
            locale = get_locale()

        if locale == "nl" and self.nl_tags:
            return self.nl_tags
        elif locale == "en" and self.en_tags:
            return self.en_tags
        else:
            return "N/A"


@event.listens_for(Committee, "load")
def set_committee_locale(committee, _):
    """
    Fill model content according to language.

    This function is called after an Activity model is filled with data from
    the database, but before is used in all other code.

    Use the locale of the current user/client to determine which language to
    display on the whole website. If the users locale is unavailable, select
    the alternative language, suffixing the title of the activity with the
    displayed language.
    """

    committee.name = committee.get_localized_name()
    committee.tags = committee.get_localized_tags()


@deprecated
@mapper_registry.mapped
class CommitteeRevision(BaseEntity):
    """
    This solely exists as a backup, use committee and page_revision instead.
    """

    __tablename__ = "committee_revision"

    nl_title = Column(String(128))
    en_title = Column(String(128))
    comment = Column(String(1024))
    nl_description = Column(Text)
    en_description = Column(Text)
    group_id = Column(Integer, ForeignKey("group.id"))
    coordinator_id = Column(Integer, ForeignKey("user.id"))
    interim = Column(Boolean)
    open_new_members = Column(Boolean, nullable=False, default=0)
    user_id = Column(Integer, ForeignKey("user.id"))
    page_id = Column(Integer, ForeignKey("page.id"))
