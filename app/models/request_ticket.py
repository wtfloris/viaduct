from sqlalchemy import Column, ForeignKey, Integer, String

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class PasswordTicket(BaseEntity):
    __tablename__ = "password_ticket"

    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    hash = Column(String(64), nullable=False)
