import string
import uuid
from typing import Dict, List, Tuple

from flask_babel import _
from jinja2 import Environment, FileSystemLoader, TemplateNotFound

from app import worker
from app.exceptions.base import ValidationException
from app.utils.validation import validate_email_address

DEFAULT_SENDER = "info@svia.nl"
DEFAULT_TEMPLATE_LOCALE = "en"


class MailCommand:
    def __init__(self, to: str) -> None:
        self.to: str = to
        self.subject: str = ""
        self.sender: str = DEFAULT_SENDER
        self.reply_to: str = ""
        self.html: str = ""
        # Dict with the name to a filepath.
        self.inline_attachments: Dict[str, str] = {}

        # List of tuples with the name and filepath.
        self.added_attachments: List[Tuple[str, str]] = []

        # List of tuples with the sid id and filepath.
        self.added_inline_attachments: List[Tuple[str, str]] = []

        # List of already loaded files
        self.loaded_attachments: List[Tuple[str, bytes, str]] = []

        self._init()

    def _init(self):
        # Insert default assets.
        self.with_inline_attachment(
            "via_logo", "app/templates/email/attachments/via_logo_with_mark.png"
        )

    def __repr__(self):
        return f"{__class__}({self.to}, {self.subject})"

    def with_template(self, template_name, locale, **kwargs):
        """
        Render the given email template with the given locale arguments.

        If passed 'sign_up' and 'nl', the template 'email/sign_up/nl.html'
        will be rendered. If there is no template for the given locale,
        the default locale (en) is used.
        """

        all_kwargs = dict({"get_cid": self._template_get_cid, "_": _}, **kwargs)

        try:
            self.subject, self.html = self._render_template_with_locale(
                template_name, locale, **all_kwargs
            )
        except TemplateNotFound:
            self.subject, self.html = self._render_template_with_locale(
                template_name, DEFAULT_TEMPLATE_LOCALE, **all_kwargs
            )

    def _render_template_with_locale(self, template, locale, **context):
        # filter locale to ascii + digits (user input).
        locale = "".join(
            filter(lambda i: i in string.ascii_letters + string.digits, locale)
        )
        full_template = "{}/{}.html".format(template, locale)

        env = Environment(
            loader=FileSystemLoader("app/templates/email"),
            # extensions = [SkipBlockExtension],
            cache_size=0,  # disable cache because we do 2 get_template()
        )

        template = env.get_template(full_template)

        title = "".join(template.blocks["title"](template.new_context(context)))
        body = template.render(context)

        return title, body

    def _template_get_cid(self, name):
        """
        Get an cid to inline in the email html.

        Register it to be send along with the email as an attachment.
        """

        if name not in self.inline_attachments:
            raise Exception(
                "Could not find an attachment with the "
                "name '{}', call with_inline_attachment('{}', "
                "filepath) first.".format(name, name)
            )

        generated_id = str(uuid.uuid4())
        self.added_inline_attachments.append(
            (generated_id, self.inline_attachments[name])
        )

        return generated_id

    def with_attachment(self, name, filepath):
        self.added_attachments.append((name, filepath))

    def with_inline_attachment(self, name, filepath):
        self.inline_attachments[name] = filepath

    def with_loaded_attachment(self, name, content, content_type):
        self.loaded_attachments.append((name, content, content_type))

    def with_html(self, html):
        self.html = html

    def validate(self):
        if not validate_email_address(self.to):
            raise ValidationException("Invalid recipient address")

        if not validate_email_address(self.sender):
            raise ValidationException("Invalid sender address")

        if not self.subject:
            raise ValidationException("Invalid subject")

        if not self.html:
            raise ValidationException("No content")


# Import google here to prevent circular dependency.
from app.utils import google  # noqa


@worker.task
def send_mail_task(command: MailCommand):
    google.send_email(command)
