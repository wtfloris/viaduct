import logging

from googleapiclient.errors import HttpError

from app import worker
from app.utils.google import build_groups_service

_logger = logging.getLogger(__name__)


@worker.task
def add_email_to_google_group(email: str, group_key: str) -> None:
    service = build_groups_service()
    if service is None:
        return
    try:
        _logger.info("Adding '%s' from google group '%s'", email, group_key)
        service.members().insert(
            groupKey=group_key, body={"email": email, "role": "MEMBER"}
        ).execute()
    except HttpError as e:
        if e.resp.status == 404:
            _logger.error(e)
        elif e.resp.status == 409:
            # Something else went wrong than the list already existing
            _logger.warning(
                "Email '%s' already in google group '%s'.", email, group_key
            )
        raise e


@worker.task
def remove_email_from_google_group(email: str, group_key: str) -> None:
    service = build_groups_service()
    if service is None:
        return
    try:
        _logger.info("Removing '%s' from google group '%s'", email, group_key)
        service.members().delete(groupKey=group_key, memberKey=email).execute()
    except HttpError as e:
        if e.resp.status == 404:
            _logger.info("'%s' is not a member of '%s' (404)", email, group_key)
            return
        raise e
