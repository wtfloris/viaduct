import logging
from celery import states

from app import app, worker
from app.service import user_service, copernica_service, user_mailing_service
from app.repository import user_repository

_logger = logging.getLogger(__name__)


@worker.task(bind=True)
def update_all_users(self) -> None:
    with app.app_context():
        user_ids = user_service.find_all_user_ids()
        self.update_state(
            state=states.STARTED, meta={"count": len(user_ids), "current": 0}
        )
        for idx, user_id in enumerate(user_ids):
            _logger.debug(
                "[%d/%d] Updating user %d's copernica details",
                idx,
                len(user_ids),
                user_id,
            )
            self.update_state(
                state=states.STARTED, meta={"count": len(user_ids), "current": idx}
            )
            user = user_service.get_user_by_id(user_id)
            copernica_service.update_profile(user)


@worker.task
def create_user(user_id: int) -> None:
    with app.app_context():
        user = user_service.get_user_by_id(user_id)
        copernica_service.create_profile(user)


@worker.task
def update_user(user_id: int) -> None:
    with app.app_context():
        user = user_service.get_user_by_id(user_id)
        copernica_service.update_profile(user)


@worker.task
def synchronize_copernica_disabling_selection_profiles() -> None:
    with app.app_context():
        selections = copernica_service.get_disabling_selections()

        for selection, selection_id in selections.items():
            _logger.info("Disabling all mailinglists for profiles in %s", selection)
            profile_ids = copernica_service.get_selection_profiles(selection_id)

            users = user_repository.find_active_mailing_users_with_copernica_id(
                profile_ids
            )

            for user in users:
                _logger.debug("Disabling all mailinglists for %s", user)
                user_mailing_service.set_mailing_list_subscriptions(
                    user, [], update_copernica=True
                )
