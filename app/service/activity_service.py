from datetime import datetime
from typing import List, Optional, Tuple, TypedDict

from flask import url_for
from unidecode import unidecode
from werkzeug.datastructures import FileStorage

from app import db
from app.api.schema import PageSearchParameters, MultilangStringDict
from app.enums import FileCategory
from app.exceptions.base import (
    BusinessRuleException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.activity import Activity
from app.models.newsletter import NewsletterActivity
from app.models.user import User
from app.repository import activity_repository
from app.service import file_service, pretix_service
from app.service.pretix_service import PretixPaymentStatus
from app.utils import google


def get_by_id(activity_id: int) -> Activity:
    activity = activity_repository.find_activity_by_id(activity_id)
    if not activity:
        raise ResourceNotFoundException("activity", activity_id)
    return activity


def get_activities_ending_after_now() -> List[Activity]:
    return activity_repository.get_activities_ending_after_now()


def activity_slug(activity):
    if activity.en_name:
        name = "".join(c for c in activity.en_name if c.isalnum())
    else:
        name = "".join(c for c in activity.nl_name if c.isalnum())
    name = unidecode(name)
    year = activity.end_time.year

    # Pretix allows for slugs up to 50, so limit name to 44 + 1 dash + 4 year
    # digits to make sure we are under the limit.
    return f"{name[:44]}-{year}"


def remove_activity(activity: Activity):
    # Ensure activity is not referenced from newsletter
    # (issue #343)
    is_used_in_newsletter = (
        db.session.query(NewsletterActivity)
        .filter(NewsletterActivity.activity_id == activity.id)
        .first()
        is not None
    )

    if is_used_in_newsletter:
        message = "Activity cannot be removed: it is used in a newsletter"
        raise BusinessRuleException(message)

    # Remove the event from google calendar
    google.delete_activity(activity.google_event_id)

    # Remove it
    db.session.delete(activity)
    db.session.commit()


def get_by_pretix_slugs(event_slugs: List[str]) -> List[Activity]:
    return activity_repository.get_activities_pretix(event_slugs)


def get_upcoming_past_for_user(user: User) -> Tuple[List[Activity], List[Activity]]:
    pretix_orders = pretix_service.get_user_orders(user, PretixPaymentStatus.active)
    pretix_event_slugs = list(pretix_orders.keys())

    # Future
    future_pretix_based_activities = activity_repository.get_upcoming_pretix(
        pretix_event_slugs
    )

    # Past
    past_pretix_based_activities = activity_repository.get_past_pretix(
        pretix_event_slugs
    )

    return (
        future_pretix_based_activities,
        past_pretix_based_activities,
    )


class ActivityApiData(TypedDict):
    name: MultilangStringDict
    description: MultilangStringDict
    start_time: datetime
    end_time: datetime
    location: str
    price: str
    pretix_event_slug: Optional[str]


def create_pretix_activity_from_pretix(
    create_data: ActivityApiData, user: User
) -> Activity:
    activity = Activity()

    # Move form fields
    activity.nl_name = create_data["name"]["nl"]
    activity.en_name = create_data["name"]["en"]
    activity.nl_description = create_data["description"]["nl"]
    activity.en_description = create_data["description"]["en"]
    activity.end_time = create_data["end_time"]
    activity.start_time = create_data["start_time"]
    activity.location = create_data["location"]
    activity.price = create_data["price"]

    # Keep track of who created the activity.
    activity.owner_id = user.id

    activity = activity_repository.flush_activity(activity)
    url = url_for("activity.get_activity", activity=activity, _external=True)

    google_activity = google.insert_activity(
        activity.nl_name,
        f"{activity.nl_description}\n\n{url}",
        activity.location,
        activity.start_time,
        activity.end_time,
    )

    if google_activity:
        activity.google_event_id = google_activity["id"]

    activity.pretix_event_slug = create_data["pretix_event_slug"]

    return activity_repository.save_activity(activity)


def edit_activity_from_pretix(activity: Activity, update_data: ActivityApiData) -> None:
    activity.nl_name = update_data["name"]["nl"]
    activity.en_name = update_data["name"]["en"]
    activity.nl_description = update_data["description"]["nl"]
    activity.en_description = update_data["description"]["en"]
    activity.start_time = update_data["start_time"]
    activity.end_time = update_data["end_time"]
    activity.location = update_data["location"]
    activity.price = update_data["price"]

    activity.pretix_event_slug = update_data["pretix_event_slug"]

    if activity.google_event_id:
        url = url_for("activity.get_activity", activity=activity, _external=True)

        google.update_activity(
            activity.google_event_id,
            activity.nl_name,
            f"{activity.nl_description}\n\n{url}",
            activity.location,
            activity.start_time,
            activity.end_time,
        )

    activity_repository.save_activity(activity)


def set_activity_picture(activity: Activity, file: FileStorage):
    if not file.mimetype.startswith("image/"):
        raise ValidationException("Invalid filetype for activity picture")

    picture_file = file_service.add_file(FileCategory.ACTIVITY_PICTURE, file)

    old_picture_id = activity.picture_file_id
    activity.picture_file_id = picture_file.id

    if old_picture_id:
        old_picture = file_service.get_file_by_id(old_picture_id)
        file_service.delete_file(old_picture)

    activity_repository.save_activity(activity)


def paginated_search_all_activities(pagination: PageSearchParameters):
    return activity_repository.paginated_search_all_activities(pagination)
