from random import choice

from app.exceptions.base import ResourceNotFoundException
from app.repository import flickr_repository


def get_album_index():
    return flickr_repository.find_album_index()


def get_album(album_id):
    album = flickr_repository.find_album(album_id=album_id)
    if not album:
        raise ResourceNotFoundException("Flickr Album", album_id)
    return album


def get_random_photo():
    album = choice(get_album_index())
    photos = get_album(album["id"])
    return choice(photos["photos"])
