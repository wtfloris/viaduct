import logging
from typing import Optional

from app import app
from app.task.mail import MailCommand, send_mail_task

_logger = logging.getLogger(__name__)


def send_mail(command: MailCommand):
    developer_mail: Optional[str] = app.config.get("DEVELOPER_MAIL")
    if app.debug or developer_mail:
        command.html = (
            f"<p>DEBUGGING: {command.to} was original address</p>" + command.html
        )
        if not developer_mail:
            _logger.warning("Missing DEVELOPER_MAIL in settings.")
            for line in command.html.splitlines():
                _logger.debug(line)
            return
        else:
            command.to = developer_mail

    command.validate()

    send_mail_task.delay(command)
