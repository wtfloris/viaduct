import logging
from functools import wraps

from authlib.integrations.flask_oauth2 import ResourceProtector, current_token
from flask import Response, flash, make_response, request
from flask_babel import lazy_gettext as _
from flask_login import current_user
from flask_restful import abort
from marshmallow import ValidationError

from app.service import role_service

_logger = logging.getLogger(__name__)


def require_role(*roles):
    def real_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            # Prioritize access_tokens over flask_login.
            user = current_token.user if current_token else None

            # If we use access_token login the user is using an api route
            api_route = True
            if user is None:
                user = current_user

                # The user is using a Flask view
                api_route = False

            has_role, need_tfa = role_service.user_has_role_and_tfa(user, *roles)

            if has_role:
                return f(*args, **kwargs)
            else:
                if need_tfa and not api_route:
                    # The user would have been authed if they had tfa enabled.
                    flash(
                        _(
                            "You need two-factor authentication "
                            "enabled to view this page."
                        ),
                        "warning",
                    )

                _logger.debug(
                    f"Denied {request.url}, user {user.id}"
                    f" missing " + " ".join(str(r) for r in roles)
                )
                return abort(403)

        return wrapper

    return real_decorator


def require_membership(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if current_user.is_anonymous or not current_user.has_paid:
            return abort(403)
        else:
            return f(*args, **kwargs)

    return wrapper


def response_headers(headers=None):
    if headers is None:
        headers = dict()

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            resp = make_response(f(*args, **kwargs))
            h = resp.headers
            for header, value in headers.items():
                h[header] = value
            return resp

        return wrapper

    return decorator


def json_schema(schema):
    def real_decorator(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            if not request.is_json:
                abort(400)
            try:
                json = request.get_json(force=True)
                return f(self, schema.load(json), *args, **kwargs)
            except ValidationError as e:
                abort(400, errors=e.messages)

        return wrapper

    return real_decorator


def form_schema(schema):
    def real_decorator(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            try:
                return f(self, schema.load(request.form), *args, **kwargs)
            except ValidationError as e:
                abort(400, errors=e.messages)

        return wrapper

    return real_decorator


def query_parameter_schema(schema):
    def real_decorator(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            try:
                query_parameters = schema.load(request.args)
                return f(self, *args, **query_parameters, **kwargs)
            except ValidationError as e:
                abort(400, errors=e.messages)

        return wrapper

    return real_decorator


class ViaResourceProtector(ResourceProtector):
    def __call__(self, scope=None, **kwargs):
        if scope:
            kwargs["scope"] = scope.name
        return super().__call__(**kwargs)


require_oauth = ViaResourceProtector()


def response(content="html", document_type="text", charset="utf-8", name=None):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            r = f(*args, **kwargs)

            resp = Response(
                r, content_type=f"{document_type}/{content}; " f"charset={charset}"
            )
            if name is not None:
                resp.headers["Content-Disposition"] = (
                    f"attachment; " f'filename="{name}"'
                )
            return resp

        return wrapper

    return decorator
