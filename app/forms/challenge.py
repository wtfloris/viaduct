from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from app.service import user_service


class ManualSubmissionForm(FlaskForm):
    # TODO Convert this to vue-async user selection, as it render slow with 2k users.
    user = QuerySelectField(
        _("User"), query_factory=user_service.find_members, get_label=lambda u: u.name
    )
