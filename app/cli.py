import os
import re

import alembic
import alembic.config
import bcrypt
import click
import sqlalchemy
from flask import current_app
from flask.cli import AppGroup
from fuzzywuzzy import fuzz
from sqlalchemy.dialects.postgresql import insert
from unidecode import unidecode

from app import app
from app.enums import ProgrammeType
from app.extensions import db
from app.models.education import Education
from app.models.group import Group
from app.models.navigation import NavigationEntry
from app.models.oauth.client import OAuthClient
from app.models.role_model import GroupRole
from app.models.setting_model import Setting
from app.models.user import User
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import group_service
from app.service.navigation_service import create_entry
from app.utils.flask_init import (
    require_login_manager,
)

administrators = AppGroup("admin", help="Add or remove users from administrators group")
app.cli.add_command(administrators)


@app.cli.command("translations")
def translations():
    i18n = "app/translations"
    config = "babel.cfg"
    messages = "messages.pot"
    os.system(
        f"pybabel extract -F {config} "
        + f"--sort-output --no-location -k lazy_gettext -o {messages} ."
    )
    os.system(f"pybabel update -i {messages} -d {i18n}")
    os.system(f"rm {messages}")
    os.system(f"pybabel compile -d {i18n}")


def _add_group(name):
    existing_group = db.session.query(Group).filter(Group.name == name).first()
    if existing_group:
        print("-> Group '{}' already exists.".format(name))
    else:
        try:
            db.session.add(Group(name, None))
            db.session.commit()
            print(f"-> Group '{name}' added.")
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()


def _add_user(user, error_msg="User already exists"):
    existing_user = db.session.query(User).filter(User.email == user.email).first()
    if existing_user:
        print("-> {}.".format(error_msg))
    try:
        db.session.add(user)
        db.session.commit()
        print(f"-> Group '{user.email}' added.")
    except sqlalchemy.exc.IntegrityError:
        db.session.rollback()
        raise


def _add_navigation(entries, parent=None):
    # Parent id of the root is None.
    if not parent:
        parent_id = None
    else:
        parent_id = parent.id

    for pos, (type, nl_title, en_title, url, children) in enumerate(entries):
        with current_app.test_request_context():
            navigation_entry = (
                db.session.query(NavigationEntry)
                .filter(NavigationEntry.en_title == en_title)
                .first()
            )

        if not navigation_entry:
            print("-> Added {}".format(en_title))
            with current_app.test_request_context():
                navigation_entry = create_entry(
                    type, nl_title, en_title, parent_id, pos + 1, url, False, False
                )
        else:
            print("-> Navigation entry {} exists".format(en_title))

        _add_navigation(children, navigation_entry)


@app.cli.command("dropdb")
def dropdb():
    db.drop_all()


@app.cli.command("createdb")
@require_login_manager
def createdb():
    """Create a new empty database with a single administrator."""

    print("* Creating database schema")

    # Create the database schema
    db.create_all()

    print("* Adding alembic stamp")

    # Create alembic_version table
    migrations_directory = current_app.extensions["migrate"].directory
    config = alembic.config.Config(os.path.join(migrations_directory, "alembic.ini"))
    config.set_main_option("script_location", migrations_directory)
    alembic.command.stamp(config, "head")

    # Add required groups
    print("* Adding administrators' and 'BC' groups")
    _add_group("administrators")
    _add_group("BC")

    # Add educations, which must be present to create the administrator user
    print("* Adding educations")
    education_names = [
        ("Informatica", "Computer Science", ProgrammeType.BACHELOR, "BSc IN"),
        (
            "Kunstmatige Intelligentie",
            "Artificial Intelligence",
            ProgrammeType.BACHELOR,
            "BSc KI",
        ),
        ("Informatiekunde", "Information Science", ProgrammeType.BACHELOR, "BSc IK"),
        ("Information Studies", "Information Studies", ProgrammeType.MASTER, "MSc IS"),
        (
            "Software Engineering",
            "Software Engineering",
            ProgrammeType.MASTER,
            "MSc SE",
        ),
        (
            "Security and Network Engineering",
            "Security and Network Engineering",
            ProgrammeType.MASTER,
            "MSc SNE",
        ),
        (
            "Artificial Intelligence",
            "Artificial Intelligence",
            ProgrammeType.MASTER,
            "MSc AI",
        ),
        (
            "Computational Science",
            "Computational Science",
            ProgrammeType.MASTER,
            "MSc CLSJD",
        ),
        ("Computer Science", "Computer Science", ProgrammeType.MASTER, "MSc CPS"),
        ("Medial Informatics", "Medial Informatics", ProgrammeType.MASTER, None),
        ("Grid Computing", "Grid Computing", ProgrammeType.MASTER, None),
        (
            "Bioinformatics and Systems Biology",
            "Bioinformatics and Systems Biology",
            ProgrammeType.MASTER,
            "MSc BSBJD",
        ),
        ("Logic", "Logic", ProgrammeType.MASTER, "MSc Logic"),
        ("Programmeren", "Programming", ProgrammeType.MINOR, "Min Prog"),
        ("Informatica", "Computer Science", ProgrammeType.MINOR, "Min IN"),
        (
            "Kunstmatige Intelligentie",
            "Artificial Intelligence",
            ProgrammeType.MINOR,
            "Min KI",
        ),
        ("Anders", "Other", ProgrammeType.OTHER, None),
    ]

    for nl_name, en_name, p_type, datanose_code in education_names:
        with current_app.test_request_context():
            if (
                not db.session.query(Education)
                .filter(
                    Education.nl_name == nl_name,
                    Education.en_name == en_name,
                    Education.programme_type == p_type,
                )
                .first()
            ):
                if datanose_code:
                    education = Education(
                        nl_name=nl_name,
                        en_name=en_name,
                        programme_type=p_type,
                        datanose_code=datanose_code,
                        is_via_programme=True,
                    )
                else:
                    education = Education(
                        nl_name=nl_name, en_name=en_name, programme_type=p_type
                    )

                db.session.add(education)
            else:
                print("-> Education {} exists".format(en_name))

    db.session.commit()

    # Add some default navigation
    print("* Adding default navigation entries")
    navigation_entries = [
        (
            "url",
            "Vereniging",
            "Association",
            "/via",
            [
                ("url", "Nieuws", "News", "/news/", []),
                ("url", "PimPy", "PimPy", "/pimpy", []),
                ("url", "Commissies", "Committees", "/commissie", []),
                (
                    "url",
                    "Admin",
                    "Admin",
                    "/admin",
                    [
                        ("url", "Navigatie", "Navigation", "/navigation", []),
                        ("url", "Formulieren", "Forms", "/forms", []),
                        ("url", "Redirect", "Redirect", "/redirect", []),
                        ("url", "Users", "Users", "/users", []),
                        ("url", "Groups", "Groups", "/groups", []),
                        ("url", "Files", "Files", "/files", []),
                    ],
                ),
            ],
        ),
        (
            "activities",
            "Activiteiten",
            "Activities",
            "/activities",
            [
                (
                    "url",
                    "Activiteiten Archief",
                    "Activities archive",
                    "/activities/archive",
                    [],
                ),
                (
                    "url",
                    "Activiteiten Overzicht",
                    "Activities overview",
                    "/activities/view",
                    [],
                ),
            ],
        ),
        ("url", "Vacatures", "Vacancies", "/vacancies/", []),
        ("url", "Tentamenbank", "Examinations", "/examination", []),
    ]

    _add_navigation(navigation_entries)

    print("* Adding administrator user")

    email_regex = re.compile(r"^[^@]+@[^@]+\.[^@]+$")
    while True:
        email = click.prompt("\tEmail")
        if email_regex.match(email):
            break
        print("\tInvalid email address: " + email)

    # Add admin user to administrators group
    admin_group = db.session.query(Group).filter_by(name="administrators").one_or_none()

    if db.session.query(User).filter_by(email=email).first() is not None:
        print(f"-> User '{email}' exists")
    else:
        passwd_plain = click.prompt(
            "\tPassword", hide_input=True, confirmation_prompt=True
        )
        first_name = click.prompt("\tFirst name")
        last_name = click.prompt("\tLast name")

        passwd = bcrypt.hashpw(passwd_plain.encode("utf-8"), bcrypt.gensalt())
        admin = User(
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=passwd,
            educations=[db.session.query(Education).first()],
            phone_nr="",
            student_id="",
            address="",
            zip="",
            city="",
            locale="en",
        )
        admin.has_paid = True
        _add_user(admin, "A user with email '{}' already exists".format(email))

        group_service.add_group_users(admin_group.id, [admin.id])
        db.session.commit()

    # Upsert the developer mail for local testing.
    print(f"* Adding {email} as DEVELOPER_MAIL setting.")
    db.session.execute(
        insert(Setting.__table__)
        .values(key="DEVELOPER_MAIL", value=email)
        .on_conflict_do_update(index_elements=["key"], set_=dict(value=email))
    )
    db.session.commit()

    roles = []
    for role in Roles:
        group_role = GroupRole()
        group_role.group_id = admin_group.id
        group_role.role = role.name
        roles.append(group_role)

    # Grant read/write privilege to administrators group on every module
    db.session.bulk_save_objects(roles)
    db.session.commit()

    print("* Adding public OAuth client")

    client_id = "public"
    client_secret = "public"
    oauth_client = (
        db.session.query(OAuthClient).filter_by(client_id=client_id).one_or_none()
    )
    if oauth_client is None:
        oauth_client = (
            db.session.query(OAuthClient)
            .filter_by(client_secret=client_secret)
            .one_or_none()
        )
    if oauth_client is not None:
        print(f"-> OAuth Client {client_id}:{client_secret} exists")
    else:
        oauth_client = OAuthClient(
            client_id=client_id,
            client_secret=client_secret,
            auto_approve=False,
        )
        oauth_client.set_client_metadata(
            {
                "client_name": "Development Client",
                "response_types": ["code", "token"],
                "scope": "\n".join(s.name for s in Scopes),
                "redirect_uris": ["https://localhost:5000"],
                "grant_types": [
                    "authorization_code",
                    "implicit",
                    "refresh_token",
                    "password",
                ],
                "token_endpoint_auth_method": "client_secret_post",
            }
        )
        db.session.add(oauth_client)
        db.session.commit()

    print("Done!")


def _administrators_action(user_search, remove):
    """Add or remove users in the administrators group."""
    admin_group = db.session.query(Group).filter(Group.name == "administrators").first()
    if admin_group is None:
        print("Administrators group does not exist.")
        return

    if user_search.isdigit():
        # Search for user ID
        user_id = int(user_search)
        user_found = db.session.query(User).get(user_id)
        if user_found is None or user_id == 0:
            print("User with ID {} does not exist.".format(user_id))
            return
    else:
        # Search in user name
        users = db.session.query(User).all()

        maximum = 0
        user_found = None

        # Find user with highest match ratio
        for user in users:
            if user.id == 0:
                continue

            first_name = unidecode(user.first_name.lower().strip())
            last_name = unidecode(user.last_name.lower().strip())

            rate_first = fuzz.ratio(first_name, user_search)
            rate_last = fuzz.ratio(last_name, user_search)

            full_name = first_name + " " + last_name
            rate_full = fuzz.ratio(full_name, user_search)

            if rate_first > maximum or rate_last > maximum or rate_full > maximum:
                maximum = max(rate_first, max(rate_last, rate_full))
                user_found = user

        if user_found is None:
            print("No user found")
            return

    print("Found user: {} (ID {})".format(user_found.name, user_found.id))
    if admin_group in user_found.groups:
        if not remove:
            print("User is already in administrators group")
            return
    elif remove:
        print("User is not in administrators group")
        return

    if remove:
        prompt = "Remove {} from administrators group?".format(user_found.name)
    else:
        prompt = "Add {} to administrators group?".format(user_found.name)

    if click.confirm(prompt):
        if remove:
            group_service.remove_group_users(admin_group.id, user_found.id)
        else:
            group_service.add_group_users(admin_group.id, user_found.id)

        db.session.commit()
        print("User successfully {}.".format("removed" if remove else "added"))


@administrators.command(help="User ID or name")
@click.argument("user")
def add(user):
    """Add a user to the administrator group."""
    _administrators_action(user, False)


@administrators.command(help="User ID or name")
@click.argument("user")
def remove(user):
    """Remove a user from the administrator group."""
    _administrators_action(user, True)
