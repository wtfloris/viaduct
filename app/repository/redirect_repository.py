from typing import Optional

from flask_sqlalchemy import Pagination
from sqlalchemy import func

from app import db
from app.api.schema import PageSearchParameters
from app.models.redirect import Redirect
from app.repository.utils.pagination import search_columns


def find_by_path(fro: str) -> Optional[Redirect]:
    return (
        db.session.query(Redirect)
        .filter(func.lower(Redirect.fro) == func.lower(fro))
        .one_or_none()
    )


def redirect_exists(fro: str, redirection: Redirect = None) -> bool:
    q = db.session.query(Redirect).filter(Redirect.fro == fro)
    if redirection is not None:
        q = q.filter(Redirect.id != redirection.id)

    return q.count() > 0


def edit_redirection(redirection: Redirect, fro: str, to: str) -> Redirect:
    redirection.fro = fro
    redirection.to = to
    db.session.commit()

    return redirection


def paginated_search_all_groups(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Redirect).order_by(Redirect.fro)

    q = search_columns(q, pagination.search, Redirect.fro, Redirect.to)

    return q.paginate(pagination.page, pagination.limit, False)
