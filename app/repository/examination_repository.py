from typing import List

from app import db
from app.models.examination import Examination


def get_all_examinations_by_course_id(course_id: int) -> List[Examination]:
    return (
        db.session.query(Examination)
        .filter_by(course_id=course_id)
        .order_by(Examination.date)
        .all()
    )


def create_examination() -> Examination:
    return Examination()
