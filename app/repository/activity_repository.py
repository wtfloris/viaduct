from datetime import datetime
from typing import List, Optional

from app import db
from app.api.schema import PageSearchParameters
from app.models.activity import Activity
from app.repository.utils.pagination import search_columns


def save_activity(activity: Activity) -> Activity:
    db.session.add(activity)
    db.session.commit()
    return activity


def flush_activity(activity: Activity) -> Activity:
    db.session.add(activity)
    db.session.flush()
    return activity


def find_activity_by_id(activity_id: int) -> Optional[Activity]:
    return db.session.query(Activity).filter_by(id=activity_id).one_or_none()


def get_activities_ending_after_now() -> List[Activity]:
    return (
        db.session.query(Activity)
        .filter(Activity.end_time > datetime.now())
        .order_by(Activity.start_time)
        .all()
    )


def get_activities_pretix(event_slugs: List[str]) -> List[Activity]:
    return (
        db.session.query(Activity)
        .filter(Activity.pretix_event_slug.in_(event_slugs))
        .order_by(Activity.start_time)
        .all()
    )


def get_upcoming_pretix(event_slugs: List[str]):
    return (
        db.session.query(Activity)
        .filter(Activity.end_time > datetime.today())
        .filter(Activity.pretix_event_slug.in_(event_slugs))
        .order_by(Activity.start_time)
        .all()
    )


def get_past_pretix(event_slugs: List[str]):
    return (
        db.session.query(Activity)
        .filter(Activity.end_time < datetime.today())
        .filter(Activity.pretix_event_slug.in_(event_slugs))
        .order_by(Activity.start_time)
        .all()
    )


def find_all():
    return db.session.query(Activity).all()


def paginated_search_all_activities(pagination: PageSearchParameters):
    q = db.session.query(Activity).order_by(Activity.id.desc())

    q = search_columns(q, pagination.search, Activity.en_name, Activity.nl_name)

    return q.paginate(pagination.page, pagination.limit, False)
