from typing import List, Optional

from flask_sqlalchemy import Pagination
from sqlalchemy import func

from app import db
from app.api.schema import PageSearchParameters
from app.models.group import Group, UserGroup, UserGroupHistory
from app.models.user import User
from app.repository.utils.pagination import search_columns


def save(group: Group):
    db.session.add(group)
    db.session.commit()


def find_by_id(group_id: int) -> Group:
    return db.session.query(Group).filter_by(id=group_id).one_or_none()


def get_group_by_name(group_name: str) -> Optional[Group]:
    return db.session.query(Group).filter_by(name=group_name).one_or_none()


def get_group_by_maillist(maillist: str) -> Optional[Group]:
    return db.session.query(Group).filter_by(maillist=maillist).one_or_none()


def find_groups() -> List[Group]:
    return db.session.query(Group).order_by(Group.name).all()


def paginated_search_all_groups(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Group).order_by(func.lower(Group.name).asc())

    q = search_columns(q, pagination.search, Group.name)

    return q.paginate(pagination.page, pagination.limit, False)


def get_groups_for_user(user: User) -> List[Group]:
    return (
        db.session.query(Group)
        .join(UserGroup)
        .filter(UserGroup.user_id == user.id)
        .order_by(Group.name)
        .all()
    )


def paginated_search_group_users(
    group_id: int, pagination: PageSearchParameters
) -> Pagination:
    q = (
        db.session.query(User)
        .join(UserGroup, UserGroup.user_id == User.id)
        .order_by(User.id.asc())
        .filter(UserGroup.group_id == group_id)
    )

    q = search_columns(
        q,
        pagination.search,
        User.email,
        User.first_name,
        User.last_name,
        User.student_id,
    )

    return q.paginate(pagination.page, pagination.limit, False)


def get_group_users(group_id: int) -> List[User]:
    return (
        db.session.query(User)
        .join(UserGroup, UserGroup.user_id == User.id)
        .filter(UserGroup.group_id == group_id)
        .all()
    )


def remove_group_users(group_id: int, user_ids: List[int]) -> None:
    for user_id in user_ids:
        user_group = (
            db.session.query(UserGroup)
            .filter(UserGroup.group_id == group_id, UserGroup.user_id == user_id)
            .one_or_none()
        )
        history = UserGroupHistory.from_user_group(user_group)
        db.session.add(history)
        db.session.delete(user_group)
    db.session.commit()


def add_group_users(group: Group, users: List[User]) -> None:
    group.users.extend(users)
    db.session.add(group)
    db.session.commit()


def create_group(name: str, mailtype: str, maillist: Optional[str]):
    g = Group(name=name, maillist=maillist, mailtype=mailtype)
    db.session.add(g)
    db.session.commit()
    return g
