from typing import List, Optional, Tuple

from flask_sqlalchemy import Pagination
from sqlalchemy import func, select
from sqlalchemy.engine import Result
from sqlalchemy.orm import raiseload

from app import db
from app.api.schema import PageSearchParameters
from app.models.user import User
from app.repository.utils.pagination import search_columns


def create_user():
    return User()


def save(user):
    db.session.add(user)
    db.session.commit()


def save_all(users):
    db.session.add_all(users)
    db.session.commit()


def find_members():
    return (
        db.session.query(User)
        .filter_by(has_paid=True)
        .order_by(User.first_name)
        .options(raiseload("*"))
        .all()
    )


def find_members_of_merit(session) -> Result:
    stmt = (
        select(
            User.id,
            User.first_name,
            User.last_name,
            func.concat(User.first_name, " ", User.last_name).label("name"),
            User.member_of_merit_date,
        )
        .where(User.member_of_merit_date.isnot(None))
        .order_by(User.member_of_merit_date)
    )
    return session.execute(stmt)


def find_all_user_ids() -> List[int]:
    r = []
    for (value,) in db.session.query(User.id).distinct():
        r.append(value)
    return r


def find_by_id(user_id):
    return db.session.query(User).filter_by(id=user_id).one_or_none()


def find_by_ids(user_ids: List[int]):
    return db.session.query(User).filter(User.id.in_(user_ids)).all()


def find_user_by_email(email):
    return db.session.query(User).filter_by(email=email).one_or_none()


def find_user_by_copernica_id(copernica_id: int) -> User:
    return db.session.query(User).filter_by(copernica_id=copernica_id).one_or_none()


def paginated_search_all_users(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(User).order_by(User.id.asc())

    q = search_columns(
        q,
        pagination.search,
        User.email,
        func.concat(User.first_name, " ", User.last_name),
        User.student_id,
    )

    return q.paginate(pagination.page, pagination.limit, False)


def find_user_by_student_id(student_id, needs_confirmed=True):
    def query_user(confirm_status):
        return (
            db.session.query(User)
            .filter_by(student_id=student_id, student_id_confirmed=confirm_status)
            .first()
        )

    confirmed_user = query_user(True)

    # If we allow unconfirmed and did not find a confirmed user,
    # try to find an unconfirmed user
    if not needs_confirmed and not confirmed_user:
        return query_user(False)

    return confirmed_user


def find_all_users_with_unconfirmed_student_id(student_id):
    return (
        db.session.query(User)
        .filter_by(student_id=student_id, student_id_confirmed=False)
        .all()
    )


def find_active_mailing_users_with_copernica_id(copernica_ids: List[int]):
    return (
        db.session.query(User)
        .filter(User.copernica_id.in_(copernica_ids))
        .filter(User.mailinglists.any())
        .all()
    )


def get_user_ids_like(potential_users: List[str]) -> List[Tuple[Optional[int], str]]:
    """Gets user.ids based on a list of full names."""

    if not potential_users:
        return []

    user_expr = func.concat(User.first_name, " ", User.last_name)

    sub = db.session.query(func.unnest(potential_users).label("name")).subquery()
    return (
        db.session.query(func.min(User.id), sub.c.name)
        .join(User, user_expr.ilike(sub.c.name), isouter=True)
        .group_by(sub.c.name)
        .all()
    )
