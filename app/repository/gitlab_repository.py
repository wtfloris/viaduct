from urllib.parse import urlencode

import requests


def create_gitlab_issue(session, project_id, data) -> requests.Response:
    url = f"https://gitlab.com/api/v4/projects/{project_id}/issues"
    url += "?" + urlencode(data)
    return session.post(url)
