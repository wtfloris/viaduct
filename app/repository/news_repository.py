import datetime
from datetime import date
from typing import List, Union

from sqlalchemy import desc, or_, true

from app import db
from app.models.news import News
from app.models.newsletter import Newsletter, NewsletterNewsItem
from app.models.user import User
from app.repository.utils.pagination import search_columns
from app.utils.pagination import Pagination


def get_news_by_user_id(user_id: int) -> List[News]:
    return db.session.query(News).filter(News.user_id == user_id).all()


def find_news_by_id(news_id):
    return db.session.query(News).filter(News.id == news_id).one_or_none()


def news_used_in_newsletter(news):
    return bool(
        db.session.query(NewsletterNewsItem)
        .join(News)
        .filter(News.id == news.id)
        .first()
    )


def delete_news(news):
    db.session.delete(news)
    db.session.commit()


def get_paginated_news(only_published: bool, user: User, page: int) -> Pagination:
    news = (
        db.session.query(News)
        .filter(or_(user.has_paid == true(), ~News.needs_paid))
        .order_by(desc(News.publish_date), desc(News.id))
    )

    if only_published:
        news = news.filter(News.publish_date <= date.today())

    return news.paginate(page, 10)


def find_all() -> List[News]:
    return db.session.query(News).all()


def paginated_search_all_news(pagination):
    q = db.session.query(News).order_by(News.id.desc())

    q = search_columns(q, pagination.search, News.en_title, News.nl_title)

    return q.paginate(pagination.page, pagination.limit, False)


def find_all_recent():
    """
    Find all news posts within two last two weeks rounded up.

    Note: Used to return all news posts of this and previous month, however
    too many posts needed to be removed during newsletter creation.
    """
    two_weeks_ago = datetime.date.today() - datetime.timedelta(days=15)
    return (
        db.session.query(News)
        .filter(News.created > two_weeks_ago)
        .order_by(News.created)
        .all()
    )


def save(news: Union[Newsletter, News]):
    db.session.add(news)
    db.session.commit()
