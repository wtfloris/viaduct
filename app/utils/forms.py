from flask import flash


# TODO No longer use this.
def flash_form_errors(form):
    for errors in form.errors.values():
        for error in errors:
            flash("%s" % error, "danger")
