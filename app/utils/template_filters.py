import re
from datetime import date, datetime
from typing import Union, Optional, Tuple, Literal
from urllib.parse import quote_plus

import bleach
from bleach_allowlist import generally_xss_safe, all_styles
from flask import Markup
from jinja2 import contextfilter
from markdown import markdown
from app.utils.markdown_summary import MarkdownSummaryExtension, SummaryOptions
from app.utils.markdown_youtube import MarkdownYoutubeExtension
from app.utils.markdown_blockquotes import MarkdownBlockquotesExtension
from pytz import timezone

from app import constants

markdown_extensions = [
    "toc",
    MarkdownYoutubeExtension(),
    MarkdownBlockquotesExtension(),
]

#  This is a combination of bleach_allowlist's print_attrs and markdown_attrs
all_allowed_attrs = {
    "*": ["id", "class", "style"],
    "img": ["src", "alt", "title", "width", "height"],
    "a": ["href", "alt", "title"],
}

HEX_COLOR_REGEX = re.compile(r"^#([a-fA-F0-9]{3}|[a-fA-F0-9]{6})$")


def markdown_filter(data):
    """Renders markdown safely.

    Marks the HTML safe for Flask to be injected in a page.
    E.g. **via** --> <strong>via</strong>"""
    return Markup(markdown_escaped_filter(data))


def markdown_escaped_filter(data):
    """Renders markdown as escaped HTML.

    E.g. **via** --> &lt;strong&gt;via&lt;/strong&gt;&lt;"""

    return markdown(
        bleach.clean(
            data,
            tags=generally_xss_safe,
            attributes=all_allowed_attrs,
            styles=all_styles,
        ),
        extensions=markdown_extensions,
    )


def markdown_summary(
    data,
    max_length: int,
    truncate_method: Literal["hard", "word"] = "hard",
    read_more: str = "",
):
    options = SummaryOptions(max_length, truncate_method, read_more)
    return bleach.clean(
        markdown(data, extensions=[MarkdownSummaryExtension(options)]),
        tags=generally_xss_safe,
        # TODO https://github.com/python/typeshed/issues/5650
        attributes=all_allowed_attrs,  # type: ignore[arg-type]
        styles=all_styles,
    )


def markdown_unsafe(data):
    """Renders Markdown unsafely."""
    return Markup(markdown(data, extensions=markdown_extensions))


def strip_tags_filter(data, *args):
    for tag in args:
        # Source: http://stackoverflow.com/a/6445849/849956
        data = re.sub(
            r"<%s(?:\s[^>]*)?(>(?:.(?!/%s>))*</%s>|/>)" % (tag, tag, tag),
            "",
            data,
            flags=re.S,
        )

    return data


def domjudge_color_to_rgb(hex_color: Optional[str]) -> Optional[Tuple[int, int, int]]:
    if not hex_color:
        return None

    match = HEX_COLOR_REGEX.match(hex_color)
    if not match:
        return None

    digits = match.group(1)
    if len(digits) == 3:
        digits = "".join(2 * digit for digit in digits)

    return int(digits[0:2], 16), int(digits[2:4], 16), int(digits[4:], 16)


def register_filters(app):
    app.add_template_filter(markdown_filter, "markdown")
    app.add_template_filter(markdown_unsafe, "markdown_unsafe")
    app.add_template_filter(markdown_escaped_filter, "markdown_escaped")
    app.add_template_filter(markdown_summary, "markdown_summary")
    app.add_template_filter(strip_tags_filter, "strip_tags")
    app.add_template_filter(quote_plus, "quote_plus")

    @app.template_filter("date")
    def date_filter(dt: Union[datetime, date], dt_format=None):
        if dt is None:
            return ""
        if isinstance(dt, datetime):
            tz = timezone("Europe/Amsterdam")
            dt = dt.astimezone(tz)
        if dt_format is None:
            dt_format = constants.DATE_FORMAT
        return dt.strftime(dt_format)

    @app.template_filter("datetime")
    def datetime_filter(dt: datetime, dt_format=None):
        if dt is None:
            return ""
        tz = timezone("Europe/Amsterdam")
        local_dt = dt.astimezone(tz)
        if dt_format is None:
            dt_format = constants.DT_FORMAT
        return local_dt.strftime(dt_format)

    @app.template_filter("call_macro")
    @contextfilter
    def call_macro_filter(context, field, macro_name, *args, **kwargs):
        return context.get_all()[macro_name](field, *args, **kwargs)

    @app.template_filter("strip_attrs")
    def strip_attrs_filter(data, *args):
        for tag in args:
            # Source: http://stackoverflow.com/a/6445849/849956
            data = re.sub(
                r'(%s=")([a-zA-Z0-9:;\.\s\(\)\-\,#]*)(")' % tag, "", data, flags=re.S
            )

        return data

    @app.template_filter("markup")
    def markup_filter(data):
        return Markup(data)

    @app.template_filter("safe_markdown")
    def safe_markdown_filter(data):
        return Markup(markdown(data, extensions=markdown_extensions))

    @app.template_filter("pimpy_minute_line_numbers")
    def pimpy_minute_line_numbers(data):
        s = ""
        for i, line in enumerate(data.split("\n")):
            s += '<a id="ln%d" class="pimpy_minute_line"/>%s</a>\n' % (i, line[:-1])
        return s

    @app.template_filter("domjudge_problem_darken_color")
    def domjudge_problem_darken_color(color: Optional[str]) -> str:
        """
        Darken a color.

        Small utility function to compute the color for the border of
        the problem name badges.

        When no color is passed, a dark border is set.
        """
        rgb = domjudge_color_to_rgb(color)
        if not rgb:
            return "#000"  # black

        r, g, b = rgb
        return "#{:0>6s}".format(
            hex(int(r * 0.5) << 16 | int(g * 0.5) << 8 | int(b * 0.5))[2:]
        )

    @app.template_filter("domjudge_problem_text_color")
    def domjudge_problem_text_color(color: Optional[str]) -> str:
        rgb = domjudge_color_to_rgb(color)
        if not rgb:
            return "#fff"  # white

        r, g, b = rgb

        # Compute brightness using formula from:
        # https://www.w3.org/TR/AERT/#color-contrast
        brightness = 1 - (0.299 * r + 0.587 * g + 0.144 * b) / 255

        if brightness < 0.5:
            # Bright color, use black for text
            return "#000"
        else:
            # Dark color, use white for text
            return "#fff"
