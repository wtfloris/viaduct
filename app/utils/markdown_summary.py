from typing import NamedTuple, Literal, Any, Tuple, List, Optional

from markdown.extensions import Extension
from markdown.treeprocessors import Treeprocessor

#  Header tags will be replaced by simple spans
HEADER_TAGS = ["h1", "h2", "h3", "h4", "h5", "h6"]

#  These tags will be fully removed
REMOVE_TAGS = ["img", "hr", "ul", "ol"]


class SummaryOptions(NamedTuple):
    max_length: int
    #  Hard break in the middle of a word, or break near the max_length
    #  on a word.
    truncate: Literal["hard", "paragraph", "word"] = "hard"

    #  Character(s) to show when breaking early
    read_more: str = ""


class MarkdownSummaryExtension(Extension):
    def __init__(self, options: SummaryOptions):
        self.options = options

    def extendMarkdown(self, md, _):  # noqa
        md.registerExtension(self)
        #  float("-inf") is this processor's priority. It should always run last.
        md.treeprocessors.register(
            Processor(md, self.options), "summary", float("-inf")
        )


class Processor(Treeprocessor):
    def __init__(self, md, options: SummaryOptions):
        super().__init__(md)
        self.max_length = options.max_length
        self.truncate = options.truncate
        self.read_more = options.read_more

        if self.truncate == "hard":
            self.truncate_func = self._hard_truncate_text
        elif self.truncate == "paragraph":
            self.truncate_func = self._paragraph_truncate_text
        else:
            self.truncate_func = self._word_truncate_text

        #  List of parent, child tuple that should be removed after processing
        #  Uses 'Any' because we don't depend on lxml ourselves.
        self.should_remove: List[Tuple[Any, Any]] = []

        #  The root element of the tree
        self.root: Optional[Any] = None

        #  Indicates if we should be allowing text in 'paragraph' mode
        self.paragraph_found = False

        #  Amount of characters we have already allowed
        self.char_count = 0

    def _replace_or_remove_elements(self, element, parent_element):
        if element.tag in HEADER_TAGS:
            element.tag = "h6"

        if element.tag in REMOVE_TAGS:
            self.should_remove.append((parent_element, element))

    def _hard_truncate_text(self, element, text_position: Literal["tail", "text"]):
        text = getattr(element, text_position)

        if not text:
            return text

        remaining_chars = self.max_length - self.char_count
        text = text[: max(0, remaining_chars)]
        self.char_count += len(text)

        return text

    def _paragraph_truncate_text(self, element, text_position: Literal["tail", "text"]):
        text = getattr(element, text_position)

        if not text:
            return text

        remaining_chars = self.max_length - self.char_count

        if remaining_chars <= 0:
            if self.paragraph_found:
                text = ""

            if self.root and element in self.root and element.tag == "p":
                self.paragraph_found = True

        self.char_count += len(text)

        return text

    def _word_truncate_text(self, element, text_position: Literal["tail", "text"]):
        text = getattr(element, text_position)

        if not text:
            return text

        remaining_chars = self.max_length - self.char_count
        new_text = text[: max(0, remaining_chars)]

        #  Check if the current text truncation was when we hit the max length
        if remaining_chars <= len(text) and len(new_text) > 0:
            remaining_text = text[max(0, remaining_chars) :]

            #  Iterate over characters in remaining text until we find either
            #  a space, new line, or the end of the text.
            for c in remaining_text:
                if c.isspace():
                    break

                new_text += c

        self.char_count += len(new_text)

        return new_text

    def _will_be_deleted(self, element) -> bool:
        for _, to_delete in self.should_remove:
            if element is to_delete:
                return True

        return False

    def _all_children_will_be_deleted(self, element) -> bool:
        return all([self._will_be_deleted(child) for child in element])

    def _handle_element(self, parent_element):
        for element in parent_element:
            self._replace_or_remove_elements(element, parent_element)

            element.text = self.truncate_func(element, "text")

            #  Recursively walk the tree,
            self._handle_element(element)

            element.tail = self.truncate_func(element, "tail")

            if (
                not element.text
                and not element.tail
                and (not len(element) or self._all_children_will_be_deleted(element))
            ):
                #  The elements have no purpose.

                #  this actually also removes tags that are allowed to
                #  not have content such as <img> and <br>, but we do
                #  not want these tags anyway so it's fine.
                self.should_remove.append((parent_element, element))

    def _append_or_set_read_more(self, text):
        #  Enables "text" to be interpreted as html
        stash = self.markdown.htmlStash.store(self.read_more)

        if type(text) is str:
            return text + stash

        return stash

    def _set_read_more_text(self, root):
        if not self.read_more:
            return

        #  The root element is a <div> tag which is later ignored when
        #  markdown_python returns the html content. Additionally, the html
        #  output will always be wrapped in (multiple) <p> tags. The read
        #  more text should be added to the tail of the very last element
        #  in the root <div>. The exception is when the last element is the
        #  last <p> tag, then it should be added to the text to not create
        #  a new paragraph.

        element = root
        while len(element):
            element = element[-1]

        if element is root[-1] and self.truncate != "paragraph":
            #  This is the last <p> tag inside the root <div>, add the text
            #  inside this element
            element.text = self._append_or_set_read_more(element.text)
        else:
            element.tail = self._append_or_set_read_more(element.tail)

    def _remove_unused_elements(self):
        for parent, child in self.should_remove:
            try:
                parent.remove(child)
            except ValueError:
                #  Element might already have been removed (ex. <img>)
                pass

    def run(self, root):
        self.root = root
        self.char_count = 0

        self._handle_element(root)
        self._remove_unused_elements()
        self._set_read_more_text(root)
