from flask import Blueprint, flash, redirect, render_template, url_for
from flask_babel import gettext as _

from app import db
from app.decorators import require_membership, require_role
from app.exceptions.base import ValidationException
from app.forms.challenge import ManualSubmissionForm
from app.models.challenge import Challenge
from app.roles import Roles
from app.service import challenge_service

blueprint = Blueprint("challenge", __name__, url_prefix="/challenge")


@blueprint.route("/", methods=["GET"])
@blueprint.route("/dashboard/", methods=["GET"])
@require_membership
def view_list():
    return render_template("vue_content.htm", title=_("Challenges"))


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<challenge:challenge>/edit/", methods=["GET"])
@require_role(Roles.CHALLENGE_WRITE)
def edit(challenge: Challenge = None):
    title = _("Create challenge")
    if challenge:
        title = _("Edit challenge")
    return render_template("vue_content.htm", title=title)


@blueprint.route(
    "/<challenge:challenge>/add-manual-submission/", methods=["GET", "POST"]
)
@require_role(Roles.CHALLENGE_WRITE)
def add_manual_submission(challenge: Challenge):
    form = ManualSubmissionForm()

    if form.validate_on_submit():
        try:
            submission = challenge_service.create_submission(
                challenge=challenge,
                user_id=form.user.data.id,
                submission=None,
                image_path=None,
            )
        except ValidationException:
            flash(
                _(
                    "Already added a submission for this user, "
                    "or the challenge is not open."
                ),
                "danger",
            )
            return render_template(
                "challenge/add_manual_submission.htm", form=form, challenge=challenge
            )

        submission.approved = True
        challenge_service.assign_points_to_user(challenge.weight, submission.user_id)

        db.session.add(submission)
        db.session.commit()

        flash(_("Submission saved successfully."), "success")

        return redirect(url_for(".view_list"))

    return render_template(
        "challenge/add_manual_submission.htm", form=form, challenge=challenge
    )
