from flask import url_for

from app.models.company_job import CompanyJob


class JobsViewModel:
    def __init__(self, job: CompanyJob) -> None:
        self.job = job

    @property
    def id(self):
        return self.job.id

    @property
    def title(self):
        title, _ = self.job.get_localized_title_description()
        return title

    @property
    def description(self):
        _, description = self.job.get_localized_title_description()
        return description

    @property
    def contract_of_service(self):
        return self.job.contract_of_service

    @property
    def company(self):
        return self.job.company

    @property
    def url(self):
        return url_for("vue.view", job_id=self.job.id)

    @property
    def logo(self):
        return url_for("company.view_logo", company_id=self.company.id)

    @property
    def short_description(self):
        """Get a shortened version of the job description."""
        characters = 140

        if len(self.description) > characters:
            lines = self.description.splitlines()
            short_content = lines.pop(0)

            while len(short_content) < characters:
                short_content += f"\n{lines.pop(0)}"

            return short_content

        return self.description
