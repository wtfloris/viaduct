from flask import url_for

from app.models.committee import Committee
from app.models.page import PageRevision


class CommitteeViewModel:
    def __init__(self, committee: Committee, rev: PageRevision, user):
        self.committee = committee
        self.rev = rev
        self.user = user

    @property
    def title(self):
        return self.committee.name

    @property
    def path(self):
        return url_for("page.get_page", page=self.rev.page)

    @property
    def email(self):
        if self.committee.group.maillist:
            return f"{self.committee.group.maillist}@svia.nl"

    @property
    def picture_path(self):
        return url_for(
            "committee.picture", committee=self.committee, picture_type="thumbnail"
        )

    @property
    def edit_url(self):
        return url_for("vue.committee_edit", committee=self.committee)

    @property
    def looking_for_new_members(self):
        return self.committee.open_new_members

    @property
    def short_description(self):
        """Get a description cut a number of characters."""
        characters = 200
        if len(self.rev.content) > characters:
            lines = self.rev.content.splitlines()
            short_content = lines.pop(0)

            while len(short_content) < characters and lines:
                short_content += f"\n{lines.pop(0)}"

            return short_content

        return self.rev.content

    @property
    def tags(self):
        return map(lambda i: i.strip(), self.committee.tags.split(","))

    @property
    def pressure(self):
        return self.committee.pressure
