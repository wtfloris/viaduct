from datetime import date

from flask import Blueprint, Response, abort, render_template
from flask_babel import lazy_gettext as _
from flask_login import current_user

from app import db
from app.decorators import require_role
from app.models.news import News
from app.roles import Roles
from app.service import news_service, role_service
from app.views.viewmodel.news import NewsViewModel

blueprint = Blueprint("news", __name__, url_prefix="/news")


# TODO Move page_nr to query parameter
# TODO https://gitlab.com/studieverenigingvia/viaduct/-/issues/872
@blueprint.route("/", methods=["GET"])
@blueprint.route("/<int:page_nr>/", methods=["GET"])
def root(page_nr=1):
    news_paginator = news_service.get_paginated_news(
        only_published=True, user=current_user, page=page_nr
    )

    news_items = [NewsViewModel(n) for n in news_paginator.items]

    can_write = role_service.user_has_role(current_user, Roles.NEWS_WRITE)
    return render_template(
        "news/list.htm",
        paginator=news_paginator,
        news_items=news_items,
        can_write=can_write,
    )


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<news:news>/edit/", methods=["GET"])
@require_role(Roles.NEWS_WRITE)
def edit(news: News = None):
    if news:
        title = _("Edit news")
    else:
        title = _("Create news")
    return render_template("vue_content.htm", title=title)


@blueprint.route("/<news:news>/view/", methods=["GET"])
def view(news: News):
    if not news.can_read():
        return abort(403)

    can_write = role_service.user_has_role(current_user, Roles.NEWS_WRITE)

    return render_template("news/view_single.htm", news=news, can_write=can_write)


@blueprint.route("/rss/", methods=["GET"])
@blueprint.route("/rss/<string:locale>/")
def rss(locale="en"):
    title = "via nieuws" if locale == "nl" else "via news"
    items = (
        db.session.query(News)
        .filter(News.publish_date <= date.today(), db.or_(db.not_(News.needs_paid)))
        .order_by(News.publish_date.desc())
        .limit(20)
    )
    rv = render_template("news/rss.xml", title=title, news_items=items, locale=locale)
    return Response(rv, mimetype="application/atom+xml")
