from datetime import datetime, timedelta
from urllib.parse import urljoin

from flask import (
    Blueprint,
    Response,
    abort,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_babel import gettext as _
from flask_login import current_user
from sqlalchemy import or_

from app import app, db
from app.decorators import require_role
from app.exceptions.base import BusinessRuleException
from app.models.activity import Activity
from app.roles import Roles
from app.service import activity_service, role_service
from app.utils.serialize_sqla import serialize_sqla
from app.views.file import send_picture
from app.views.viewmodel.activity import ActivityViewModel

blueprint = Blueprint("activity", __name__, url_prefix="/activities")


# Overview of activities
# TODO Move page parameter to query parameters.
@blueprint.route("/", methods=["GET"])
@blueprint.route("/<any(archive):archive>/", methods=["GET"])
@blueprint.route("/list/<int:page_nr>/", methods=["GET"])
@blueprint.route("/list/<any(archive):archive>/<int:page_nr>/", methods=["GET"])
def view(archive=None, page_nr=1):
    search = request.args.get("search", "")
    query = db.session.query(Activity)
    if archive == "archive":
        if current_user.is_anonymous:
            abort(403)
        query = query.filter(Activity.end_time < datetime.today())
        query = query.order_by(Activity.start_time.desc())
    else:
        query = query.order_by(Activity.start_time.asc())
        query = query.filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))

    if search:
        search_key = "%{}%".format(search)
        query = query.filter(
            or_(Activity.en_name.ilike(search_key), Activity.nl_name.ilike(search_key))
        )

    can_write = role_service.user_has_role(current_user, Roles.ACTIVITY_WRITE)

    # TODO convert to page query parameter
    pagination = query.paginate(page_nr, 30, False)
    pagination.items = [ActivityViewModel(a, current_user) for a in pagination.items]

    return render_template(
        "activity/view.htm",
        activities=pagination,
        archive=archive,
        can_write=can_write,
        search=search,
    )


# TODO https://gitlab.com/studieverenigingvia/viaduct/-/issues/857
@blueprint.route("/<activity:activity>/remove/", methods=["POST"])
@require_role(Roles.ACTIVITY_WRITE)
def remove_activity(activity):
    try:
        activity_service.remove_activity(activity)
        flash(_("Activity has been removed successfully"))
    except BusinessRuleException:
        flash(
            _("Unable to delete activity: it is referenced by a newsletter"), "danger"
        )

    return redirect(url_for("activity.view"))


@blueprint.route("/<activity:activity>/", methods=["GET"])
def get_activity(activity: Activity):
    """Activity register and update endpoint.

    Register and update for an activity, with handling of custom forms
    and payment.
    """
    if activity.is_in_past() and current_user.is_anonymous:
        abort(403)

    if activity.is_in_past() and current_user.is_anonymous:
        abort(403)

    view_model = ActivityViewModel(activity, current_user)

    return view_model.render()


@blueprint.route("/create/", methods=["GET"])
@require_role(Roles.ACTIVITY_WRITE)
def create_pretix():
    title = _("Create activity")
    return render_template("vue_content.htm", title=title)


@blueprint.route("/<activity:activity>/pretix/", methods=["GET"])
def pretix_view(activity: Activity):
    if not activity.pretix_event_slug:
        abort(404)
    host = app.config["PRETIX_HOST"]
    organizer = app.config["PRETIX_ORGANIZER"]
    slug = activity.pretix_event_slug
    return redirect(urljoin(host, f"/control/event/{organizer}/{slug}/"))


@blueprint.route("/<activity:activity>/edit/", methods=["GET"])
@require_role(Roles.ACTIVITY_WRITE)
def edit(activity: Activity):
    title = _("Edit") + " " + str(activity.name)
    return render_template("vue_content.htm", title=title)


# TODO Word deze route door iets uberhaupt gebruikt?
@blueprint.route("/export/", methods=["GET"])
def export_activities():
    activities = (
        db.session.query(Activity)
        .filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))
        .order_by(Activity.start_time.asc())
        .all()
    )
    return jsonify(data=serialize_sqla(activities))


@blueprint.route("/rss/", methods=["GET"])
@blueprint.route("/rss/<string:locale>/")
def rss(locale="en"):
    title = "via activiteiten" if locale == "nl" else "via activities"
    activities = (
        db.session.query(Activity)
        .filter(Activity.end_time > (datetime.now() - timedelta(hours=12)))
        .order_by(Activity.start_time.asc())
        .all()
    )

    rv = render_template(
        "activity/rss.xml", title=title, activities=activities, locale=locale
    )
    return Response(rv, mimetype="application/atom+xml")


@blueprint.route("/<activity:activity>/picture/")
@blueprint.route("/<activity:activity>/picture/<picture_type>/")
def picture(activity: Activity, picture_type=None):
    if activity.picture_file_id is None or (
        current_user.is_anonymous and activity.is_in_past()
    ):
        return redirect("/static/img/via_activity_default.png")

    return send_picture(
        activity.picture_file_id, f"activity_picture_{activity.name}", picture_type
    )
