from flask import Blueprint, render_template
from flask_babel import _

from app.decorators import require_role
from app.roles import Roles

blueprint = Blueprint("navigation", __name__, url_prefix="/navigation")


@blueprint.route("/")
@require_role(Roles.NAVIGATION_WRITE)
def view():
    return render_template("vue_content.htm", title=_("Navigation"))


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<navigation_entry:entry>/edit/", methods=["GET"])
@require_role(Roles.NAVIGATION_WRITE)
def edit(entry=None):
    if entry:
        title = _("Edit navigation entry")
    else:
        title = _("Create navigation entry")
    return render_template("vue_content.htm", title=title)
