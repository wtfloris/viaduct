import logging

import flask
import requests
from flask import Blueprint, abort

from app.service.slide_service import fetch_departures
from config import Config

blueprint = Blueprint("slides", __name__, url_prefix="/slides")

BUS_LOCATION = ""
TRAIN_LOCATION = ""

_train_cache = None
_bus_cache = None
_fails = 0
_logger = logging.getLogger(__name__)


@Config.post_load
def _slide_config(config: Config):
    global BUS_LOCATION, TRAIN_LOCATION
    BUS_LOCATION = config.OV_BUS_LOCATION
    TRAIN_LOCATION = config.OV_TRAIN_LOCATION


@blueprint.route("/9292/")
def slide_9292():
    # The 9292 API seems to be very unstable, thus we cache it responses.
    global _train_cache, _bus_cache, _fails
    try:
        _train_cache = fetch_departures("train", TRAIN_LOCATION)
        _bus_cache = fetch_departures("bus", BUS_LOCATION)
        _fails = 0
    except requests.exceptions.RequestException as e:
        _fails += 1
        if _fails > 3:
            _logger.exception("9292 API failed too many times.", exc_info=e)

    if not _train_cache or not _bus_cache:
        abort(503)

    return flask.render_template(
        "slides/ovslide.html", departures_left=_train_cache, departures_right=_bus_cache
    )
