from flask import Blueprint, abort, render_template
from flask_babel import gettext

from app.decorators import require_role
from app.roles import Roles
from app.service import company_service, file_service
from app.views.file import send_file_inline

blueprint = Blueprint("company", __name__, url_prefix="/companies")


@blueprint.route("/admin/")
@blueprint.route("/admin/<path:_>")
@require_role(Roles.COMPANY_WRITE)
def admin(_: str = ""):
    return render_template("vue_content.htm", title=gettext("Companies"))


@blueprint.route("/<path:company_slug>/", methods=["GET"])
def view(company_slug=None):
    """View a company."""

    company = company_service.get_company_by_slug(company_slug)
    profile = company_service.get_company_profile_by_company_id(company.id)
    if not profile.active:
        abort(410)
    return render_template("company/view.htm", company=company, profile=profile)


@blueprint.route("/<int:company_id>/logo/", methods=["GET"])
def view_logo(company_id: int):
    company = company_service.get_company_by_id(company_id)

    if company.logo_file_id is None:
        return abort(404)

    logo_file = file_service.get_file_by_id(company.logo_file_id)

    filename = f"logo_{company.name}.{logo_file.extension}"
    return send_file_inline(logo_file, filename)
