from flask import Blueprint
from werkzeug.routing import BaseConverter

from app import app
from app.decorators import response
from app.exceptions.base import ValidationException
from app.service import calendar_service

blueprint = Blueprint("calendar", __name__, url_prefix="/calendar")

MEETING_TYPES = ("meeting", "alv", "activity")


class MeetingTypeConverter(BaseConverter):
    def to_python(self, value):
        values = super().to_python(value).split("+")

        invalid_types = [v for v in values if v not in MEETING_TYPES]

        if invalid_types:
            raise ValidationException(
                f"Calendar(s): {', '.join(invalid_types)} do not exist!"
            )

        return [v for v in values if v in MEETING_TYPES]

    def to_url(self, values):
        return super().to_url("+".join(values))


app.url_map.converters["meeting_type"] = MeetingTypeConverter


@blueprint.route("/<meeting_type:types>/", methods=["GET"])
@blueprint.route("/<meeting_type:types>/<string:locale>/", methods=["GET"])
@response(content="calendar", name="via_public_calendar.ics")
def public_ics(types, locale=None):
    return calendar_service.CalendarBuilder().build_public(types=types, locale=locale)


# This url is already in use in production, but unnecessary with the above route
@blueprint.route("/meetings/", methods=["GET"])
@response(content="calendar", name="via_meetings.ics")
def meetings_ics():
    return calendar_service.CalendarBuilder().build_public(types=["meeting"])
