from flask import Blueprint, render_template, redirect
from flask_login import current_user

from app.models.committee import Committee
from app.roles import Roles
from app.service import committee_service, role_service
from app.views.file import send_picture
from app.views.viewmodel.committee import CommitteeViewModel

blueprint = Blueprint("committee", __name__)


@blueprint.route("/commissie/", methods=["GET"])
def list():
    committees_with_revs = committee_service.get_all_committees_with_page_revisions()

    committees_with_revs.sort(key=lambda r: r[0].name)
    revisions = [
        CommitteeViewModel(i[0], i[1], current_user) for i in committees_with_revs
    ]
    can_write = role_service.user_has_role(current_user, Roles.GROUP_WRITE)
    return render_template(
        "committee/list.htm", revisions=revisions, can_write=can_write
    )


@blueprint.route("/committee/picture/<committee:committee>/")
@blueprint.route("/committee/picture/<committee:committee>/<picture_type>/")
def picture(committee: Committee, picture_type=None):
    if committee.picture_file_id is None:
        return redirect("/static/img/via_committee_default.jpg")

    return send_picture(
        committee.picture_file_id,
        f"committee_picture_{committee.en_name}",
        picture_type,
        thumbnail_size=(800, 600),
    )
