from flask import Blueprint, render_template
from flask_babel import lazy_gettext as _

from app.decorators import require_role
from app.roles import Roles

blueprint = Blueprint("pages", __name__, url_prefix="/pages")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.PAGE_WRITE)
def root():
    # With keep_alive_include="component" we tell the vue application to
    # only cache that component. For the pages components we cache
    # the page list to keep the scroll position, index and search active.
    return render_template(
        "vue_content.htm", title=_("Pages"), keep_alive_include="page-overview"
    )


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<page:page>/edit/", methods=["GET"])
@require_role(Roles.PAGE_WRITE)
def view(page=None):
    if page:
        title = _("Edit page")
    else:
        title = _("Create page")
    return render_template(
        "vue_content.htm", title=title, keep_alive_include="page-overview"
    )
