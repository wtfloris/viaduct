#!/usr/bin/env python

from flask import Blueprint, flash, redirect, session, url_for
from flask_babel import _, refresh
from flask_login import current_user

from app import constants, db
from app.extensions import cache
from app.views import get_safe_redirect_url

blueprint = Blueprint("lang", __name__, url_prefix="/lang")


@blueprint.route("/set/<path:lang>/", methods=["GET"])
def set_user_lang(lang=None):
    if lang not in constants.LANGUAGES.keys():
        flash(_("Language unsupported on this site") + ": " + lang, "warning")
        return redirect(get_safe_redirect_url())
    if current_user.is_anonymous:
        flash(_("You need to be logged in to set a permanent language."))
        return redirect(get_safe_redirect_url())

    current_user.locale = lang
    if "lang" in session:
        del session["lang"]
    db.session.add(current_user)
    db.session.commit()
    refresh()
    return redirect(get_safe_redirect_url())


@blueprint.route("/<path:lang>/", methods=["GET"])
def set_lang(lang=None):
    if lang not in constants.LANGUAGES.keys():
        flash(_("Language unsupported on this site") + ": " + lang, "warning")
        return redirect(get_safe_redirect_url())

    session["lang"] = lang
    if current_user.is_authenticated:
        msg = _(
            "{} is now set as language for this session. To make this "
            "setting permanent, <a href='{}'>click here</a>"
        )
        flash(
            msg.format(
                constants.LANGUAGES[lang], url_for("lang.set_user_lang", lang=lang)
            ),
            "safe",
        )

    # Clear the cache to make sure translations are recalculated.
    cache.clear()
    return redirect(get_safe_redirect_url())
