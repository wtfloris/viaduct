import json
import random
import re
from datetime import date
from typing import List, Literal

from flask import Blueprint, make_response, redirect, render_template
from flask_login import current_user
from sqlalchemy import desc

from app import app, db
from app.models.news import News
from app.service import activity_service, company_service, privacy_service
from app.views.viewmodel.activity import ActivityViewModel
from app.views.viewmodel.jobs import JobsViewModel
from app.views.viewmodel.news import NewsViewModel

blueprint = Blueprint("home", __name__)


@blueprint.route("/", methods=["GET"])
def home():
    jobs = company_service.find_all_jobs(filter_inactive=True)

    random.seed(date.today())
    indices = [i for i in range(len(jobs))]
    random.shuffle(indices)
    jobs_view_models = [JobsViewModel(jobs[i]) for i in indices[:4]]

    activities = activity_service.get_activities_ending_after_now()

    activity_view_models = [ActivityViewModel(a, current_user) for a in activities[:4]]

    news: List[News] = (
        db.session.query(News)
        .filter(
            News.publish_date <= date.today(),
            db.or_(current_user.has_paid, db.not_(News.needs_paid)),
        )
        .order_by(desc(News.publish_date), desc(News.id))
        .limit(8)
        .all()
    )

    news_view_models = [NewsViewModel(n) for n in news]

    return render_template(
        "home/home.htm",
        activities=activity_view_models,
        title="Homepage",
        news=news_view_models,
        jobs=jobs_view_models,
    )


@blueprint.route("/privacy/policy/<any(nl,en):lang>/")
def privacy_policy(lang: Literal["en", "nl"]):
    policies = privacy_service.find_policies()
    if lang == "en":
        return redirect(policies.url_en)
    if lang == "nl":
        return redirect(policies.url_nl)


rule_parser = re.compile(r"<(.+?)>")
splitter = re.compile(r"<.+?>")


@blueprint.route("/jsglue.js")
def serve_js():
    """
    Based on https://github.com/stewartpark/Flask-JSGlue

    The code is inlined, because the pypi is not up-to-date and using the git version
    results in unknown template directories. This may be fixed after a new version
    is published and installable to site-package.
    """
    output = []
    for r in app.url_map.iter_rules():
        endpoint = r.endpoint
        if app.config["APPLICATION_ROOT"] == "/" or not app.config["APPLICATION_ROOT"]:
            rule = r.rule
        else:
            rule = "{root}{rule}".format(
                root=app.config["APPLICATION_ROOT"], rule=r.rule
            )
        rule_args = [x.split(":")[-1] for x in rule_parser.findall(rule)]
        rule_tr = splitter.split(rule)
        output.append((endpoint, rule_tr, rule_args))

    rules = sorted(output, key=lambda x: len(x[1]), reverse=True)

    return make_response(
        render_template("js_bridge.js", namespace="Flask", rules=json.dumps(rules)),
        200,
        {"Content-Type": "text/javascript"},
    )
