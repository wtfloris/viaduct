from functools import wraps

from flask import Blueprint, render_template, request
from werkzeug.routing import BaseConverter, ValidationError

from app import app, get_locale
from app.exceptions.base import ResourceNotFoundException
from app.service import copernica_service, mailinglist_service

blueprint = Blueprint("copernica", __name__)


class CopernicaWebhookVerificationConverter(BaseConverter):
    """
    Converter to dynamically determine the copernica webhook verification url

    This converter has a high weight (300) to have a low priority. Pages and
    redirects have higher priority. (200 and 201 respectively).
    """

    weight = 300

    def to_python(self, value):  # pylint: disable=no-self-user
        if (
            not app.config["COPERNICA_WEBHOOK_VERIFICATION_ENABLED"]
            or value != app.config["COPERNICA_WEBHOOK_VERIFICATION_FILENAME"]
        ):
            raise ValidationError()

        return value

    def to_url(self, value):  # pylint: disable=no-self-user
        if not app.config["COPERNICA_WEBHOOK_VERIFICATION_ENABLED"]:
            raise ValidationError()

        return app.config["COPERNICA_WEBHOOK_VERIFICATION_FILENAME"]


app.url_map.converters[
    "copernica_webhook_verification_filename"
] = CopernicaWebhookVerificationConverter

# We use an auth_token since this is way easier to implement
# than to implement the intended security check correctly:
# https://www.copernica.com/en/documentation/webhook-security


def correct_copernica_webhook_token(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get("auth_token")
        if token and token == app.config["COPERNICA_WEBHOOK_TOKEN"]:
            return f(*args, **kwargs)
        else:
            # Don't give away whether auth_token was correct
            return ""

    return wrapper


@blueprint.route("/<copernica_webhook_verification_filename:filename>")
def verification(filename):  # pylint: disable=unused-argument
    return (
        app.config["COPERNICA_WEBHOOK_VERIFICATION_CONTENT"],
        {"Content-Type": "text/plain"},
    )


@blueprint.route("/copernica/webhook/", methods=["POST"])
@correct_copernica_webhook_token
def webhook():
    if request.form is not None:
        copernica_service.handle_webhook(request.form)

    return ""


@blueprint.route(
    "/copernica/unsubscribed/<int:mailinglist_id>/<int:profile_id>/<profile_secret>/"
)
def unsubscribed_landingpage(mailinglist_id: int, profile_id: int, profile_secret: str):
    def error():
        return render_template("copernica/unsubscribed.htm", error=True), 400

    try:
        mailinglist = mailinglist_service.get_mailinglist(mailinglist_id)
    except ResourceNotFoundException:
        return error()

    profile = copernica_service.get_profile_info(profile_id)
    if not profile or profile.secret != profile_secret:
        return error()

    mailinglist_name = mailinglist_service.get_mailinglist_name(
        mailinglist, get_locale()
    )

    return render_template(
        "copernica/unsubscribed.htm", error=False, mailinglist_name=mailinglist_name
    )
