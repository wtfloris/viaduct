from functools import wraps

from flask import (
    Blueprint,
    Response,
    abort,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_babel import lazy_gettext as _

from app import app, db
from app.decorators import require_role
from app.models.newsletter import Newsletter
from app.roles import Roles
from app.service import page_service

blueprint = Blueprint("newsletter", __name__, url_prefix="/newsletter")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.NEWS_WRITE)
def root():
    newsletters = db.session.query(Newsletter).order_by(Newsletter.created.desc()).all()
    auth_token = app.config["COPERNICA_NEWSLETTER_TOKEN"]
    return render_template(
        "newsletter/view.htm", newsletters=newsletters, token=auth_token
    )


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<newsletter:newsletter>/edit/", methods=["GET"])
@require_role(Roles.NEWS_WRITE)
def edit(newsletter=None):
    if newsletter:
        title = _("Edit newsletter")
    else:
        title = _("Create newsletter")
    return render_template("vue_content.htm", title=title)


@blueprint.route("/<newsletter:newsletter>/remove/", methods=["GET", "POST"])
@require_role(Roles.NEWS_WRITE)
def delete(newsletter):
    # TODO CSRF protect https://gitlab.com/studieverenigingvia/viaduct/-/issues/862
    if request.method == "GET":
        return render_template("newsletter/confirm.htm")

    db.session.delete(newsletter)
    db.session.commit()

    return redirect(url_for(".root"))


def correct_token_provided(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get("auth_token")
        if token and app.config["COPERNICA_NEWSLETTER_TOKEN"] == token:
            return f(*args, **kwargs)
        else:
            return abort(403)

    return wrapper


@blueprint.route("/latest/committees/", methods=["GET"])
@correct_token_provided
def committees_xml():
    pages = page_service.find_all_committee_pages()
    revisions = [page_service.get_latest_revision(page) for page in pages]
    revisions.sort(key=lambda r: r.title)
    new_members = [c for c in revisions if c.open_new_members]

    return Response(
        render_template("newsletter/committees.xml", items=new_members),
        mimetype="text/xml",
    )


@blueprint.route("/<newsletter:newsletter>/activities/", methods=["GET"])
@blueprint.route("/latest/activities/", methods=["GET"])
@correct_token_provided
def activities_xml(newsletter=None):
    if newsletter is None:
        newsletter = db.session.query(Newsletter).order_by(Newsletter.id.desc()).first()
    return Response(
        render_template("newsletter/activities.xml", items=newsletter.activities),
        mimetype="text/xml",
    )


@blueprint.route("/<newsletter:newsletter>/news/", methods=["GET"])
@blueprint.route("/latest/news/", methods=["GET"])
@correct_token_provided
def news_xml(newsletter=None):
    if newsletter is None:
        newsletter = db.session.query(Newsletter).order_by(Newsletter.id.desc()).first()
    return Response(
        render_template("newsletter/news.xml", items=newsletter.news_items),
        mimetype="text/xml",
    )
