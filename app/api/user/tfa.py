from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response, request
from flask_restful import Resource
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth
from app.exceptions.base import ValidationException
from app.service import user_service, user_tfa_service


class UserGetTfaUriSchema(RestSchema):
    provisioning_uri = fields.String()
    secret = fields.String()


class UserHasTfaSchema(RestSchema):
    tfa_enabled = fields.Boolean()


class UserPostEnableTfaSchema(RestSchema):
    disable_tfa = fields.Boolean(required=False)
    secret = fields.String(required=False)
    otp = fields.String(required=True)


class UserTfaResource(Resource):
    schema_get = UserHasTfaSchema()
    schema_post = UserGetTfaUriSchema()

    schema_post_patch = UserPostEnableTfaSchema()

    @require_oauth()
    def get(self):
        """Gets if the user has tfa enabled."""
        user = user_service.get_user_by_id(current_token.user.id)
        enabled = bool(user.tfa_enabled)
        return self.schema_get.dump(dict(tfa_enabled=enabled))

    @require_oauth()
    def post(self):
        """Starts the process of enabling TFA by generating a secret."""
        secret, uri = user_tfa_service.get_two_factor_provisioning_uri(
            current_token.user.id, request.host
        )
        return self.schema_post.dump(dict(provisioning_uri=uri, secret=secret))

    @json_schema(schema_post_patch)
    @require_oauth()
    def patch(self, data):
        """Enables TFA by confirming that the user can generate a OTP"""

        if data.get("disable_tfa"):
            user_tfa_service.disable_two_factor_authentication(
                current_token.user.id, data.get("otp")
            )

            return Response(status=HTTPStatus.NO_CONTENT)
        elif data.get("secret"):
            user_tfa_service.enable_two_factor_authentication(
                current_token.user.id, data.get("secret"), data.get("otp")
            )

            return Response(status=HTTPStatus.NO_CONTENT)
        else:
            raise ValidationException("Secret or disable_tfa missing " "from request.")
