from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response, abort
from flask_restful import Resource
from marshmallow import fields, pre_dump
from marshmallow.validate import OneOf

from app.api.schema import (
    AutoMultilangStringField,
    PageSearchParameters,
    PaginatedResponseSchema,
    PaginatedSearchSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.exceptions.base import ValidationException
from app.models.page import Page, PageRevision
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import page_service


class PagePreviewPostSchema(RestSchema):
    content = fields.Str(required=True)


class PageSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    path = fields.String(required=True)
    deleted = fields.DateTime(dump_only=True)
    type = fields.String(validate=OneOf(("page", "committee")))
    require_membership_to_view = fields.Boolean(required=True)

    @classmethod
    def get_list_schema(cls):
        return cls(many=True)

    @classmethod
    def get_update_schema(cls):
        return cls(only=("id", "path", "deleted", "require_membership_to_view"))

    @pre_dump
    def wrap_page(self, in_data, **kwargs):
        in_data.require_membership_to_view = in_data.needs_paid
        return in_data


class PageResource(Resource):
    schema_get = PageSchema()
    schema_put = PageSchema.get_update_schema()

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    def get(self, page: Page):
        return self.schema_get.dump(page)

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    @json_schema(schema_put)
    def put(self, page_update: dict, page: Page):
        page = page_service.edit_page(
            page, page_update["path"], page_update["require_membership_to_view"]
        )
        return self.schema_get.dump(page)

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    def delete(self, page: Page):
        page_service.delete_page(page)
        return Response(status=HTTPStatus.NO_CONTENT)


class PageListResource(Resource):
    schema_get = PaginatedResponseSchema(PageSchema.get_list_schema())
    schema_search = PaginatedSearchSchema()
    schema_post = PageSchema()

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    @query_parameter_schema(schema_search)
    def get(self, pagination: PageSearchParameters):
        pagination_result = page_service.paginated_search_all_pages(pagination)
        return self.schema_get.dump(pagination_result)

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    @json_schema(schema_post)
    def post(self, page: dict):
        page_created = page_service.create_page(
            page["type"], page["path"], page["require_membership_to_view"]
        )
        return self.schema_post.dump(page_created)


class PageRevisionSchema(RestSchema):
    page_id = fields.Integer(dump_only=True)
    title = AutoMultilangStringField(required=True)
    content = AutoMultilangStringField(required=True)
    revision_comment = fields.String(read_only=True)

    @pre_dump
    def wrap_revision(self, in_data: PageRevision, **kwargs):
        return {
            "en_title": in_data.en_title,
            "nl_title": in_data.nl_title,
            "nl_content": in_data.nl_content,
            "en_content": in_data.en_content,
            "revision_comment": in_data.comment,
        }


class PageRevisionResource(Resource):
    schema = PageRevisionSchema()

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    @json_schema(schema)
    def post(self, details, page_id: int):
        page = page_service.get_page_by_id(page_id, incl_deleted=True)

        page_service.create_page_revision(
            page,
            details["title"],
            details["revision_comment"],
            details["content"],
            current_token.user,
        )

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    def get(self, page_id: int, revision_id: int = -1):
        page = page_service.get_page_by_id(page_id, incl_deleted=True)

        if revision_id < 0:
            rev = page_service.get_latest_revision(page)
        else:
            raise NotImplementedError()

        return self.schema.dump(rev)


class PagePreviewResource(Resource):
    schema_post = PagePreviewPostSchema()

    @require_oauth(Scopes.page)
    @require_role(Roles.PAGE_WRITE)
    @json_schema(schema_post)
    def post(self, details):
        return page_service.preview_content(details["content"])


class PageRenderResource(Resource):
    @require_oauth(Scopes.page, optional=True)
    def get(self, lang, path):
        page = page_service.get_page_by_path(path)

        # User is not logged in and page is for members only
        if page.needs_paid and not current_token:
            return abort(401)

        # User does not have permission to see the content of the page
        if current_token and not page_service.can_user_read_page(
            page, current_token.user
        ):
            return abort(403)

        # User is logged in and has permission, or page does not need permission
        revision = page_service.get_latest_revision(page)

        if lang == "en":
            return {
                "page_id": page.id,
                "title": revision.en_title,
                "content": page_service.preview_content(revision.en_content),
            }
        elif lang == "nl":
            return {
                "page_id": page.id,
                "title": revision.nl_title,
                "content": page_service.preview_content(revision.nl_content),
            }
        else:
            raise ValidationException("Unknown language")
