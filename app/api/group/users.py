from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response
from flask_restful import Resource, abort
from marshmallow import fields

from app.api.schema import PaginatedResponseSchema, PaginatedSearchSchema, RestSchema
from app.api.user.user import UserSchema
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import group_service, role_service


class UserIdListGroupUserSchema(RestSchema):
    user_ids = fields.List(fields.Integer(), required=True)


class GroupUserListResource(Resource):
    schema_get = PaginatedResponseSchema(UserSchema.get_list_schema())
    schema_search = PaginatedSearchSchema()
    schema_post_delete = UserIdListGroupUserSchema()

    @require_oauth(Scopes.group)
    @query_parameter_schema(schema_search)
    def get(self, pagination, group_id):
        has_role = role_service.user_has_role(current_token.user, Roles.GROUP_READ)
        in_group = group_service.user_member_of_group(current_token.user, group_id)

        if has_role or in_group:
            pagination = group_service.paginated_search_group_users(
                group_id, pagination
            )

            return self.schema_get.dump(pagination)
        else:
            return abort(403)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema_post_delete)
    def post(self, data, group_id: int):
        user_ids = data.get("user_ids")
        group_service.add_group_users(group_id, user_ids)
        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema_post_delete)
    def delete(self, data, group_id: int):
        user_ids = data.get("user_ids")
        group_service.remove_group_users(group_id, user_ids)
        return Response(status=HTTPStatus.NO_CONTENT)
