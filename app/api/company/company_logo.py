from http import HTTPStatus

from flask import Response
from flask_restful import Resource, reqparse
from werkzeug.datastructures import FileStorage

from app.decorators import require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service


class CompanyLogoResource(Resource):
    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def put(self, company_id: int):
        parse = reqparse.RequestParser()
        parse.add_argument("file", type=FileStorage, location="files")
        args = parse.parse_args()
        logo: FileStorage = args["file"]

        company_service.set_logo(company_id=company_id, logo=logo)
        return Response(status=HTTPStatus.NO_CONTENT)
