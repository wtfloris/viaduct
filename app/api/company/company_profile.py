from flask_restful import Resource
from marshmallow import fields

from app.api.company.company import CompanyModuleSchemaMixin
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service


class CompanyProfileSchema(RestSchema, CompanyModuleSchemaMixin):
    description_nl = fields.String()
    description_en = fields.String()


class CompanyProfileListSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    company_id = fields.Integer(dump_only=True)
    company_name = fields.String(dump_only=True)
    company_slug = fields.String(dump_only=True)


class CompanyProfileListResource(Resource):
    """Public API, so no scope or role required."""

    schema = CompanyProfileListSchema(many=True)

    def get(self):
        profiles = company_service.find_all_profiles()
        profiles = [
            {
                "id": profile.id,
                "company_id": profile.company_id,
                "company_name": profile.company.name,
                "company_slug": profile.company.slug,
            }
            for profile in profiles
        ]
        return self.schema.dump(profiles)


class CompanyProfileResource(Resource):
    schema = CompanyProfileSchema()

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def get(self, company_id):
        profile = company_service.get_company_profile_by_company_id(company_id)
        return self.schema.dump(profile)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def post(self, company_profile: dict, company_id: int):
        profile = company_service.create_company_profile(
            company_id=company_id, **company_profile
        )
        return self.schema.dump(profile)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, company_profile: dict, company_id: int):
        profile = company_service.edit_company_profile(
            company_id=company_id, **company_profile
        )
        return self.schema.dump(profile)
