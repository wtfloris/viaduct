from authlib.integrations.flask_oauth2 import current_token
from flask_restful import Resource, abort
from marshmallow import fields, pre_dump

from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service, role_service


class CompanyModuleSchemaMixin:
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    company_id = fields.Integer(dump_only=True)
    active = fields.Boolean(dump_only=True)
    enabled = fields.Boolean(required=True)
    start_date = fields.Date(required=True)
    end_date = fields.Date(required=True)


class CompanySchema(RestSchema):

    id = fields.Integer(dump_only=True)

    name = fields.String(required=True)
    website = fields.URL(required=True)
    slug = fields.String(required=True)
    logo = fields.Boolean(dump_only=True)

    active = fields.Boolean(dump_only=True)

    contract_start_date = fields.Date()
    contract_end_date = fields.Date()

    @pre_dump
    def transform_fields(self, company, **kwargs):
        company.logo = bool(company.logo_file_id)
        return company

    @classmethod
    def get_list_schema(cls):
        return cls(many=True, only=("id", "name", "website", "slug"))


class CompanyResource(Resource):
    schema = CompanySchema()

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def get(self, company_id: int):
        if company_id:
            company = company_service.get_company_by_id(company_id)
        else:
            return abort(400)

        return self.schema.dump(company)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, company_update, company_id: int):
        company = company_service.edit_company(
            company_id=company_id,
            name=company_update.get("name"),
            slug=company_update.get("slug"),
            website=company_update.get("website"),
            contract_start_date=company_update.get("contract_start_date"),
            contract_end_date=company_update.get("contract_end_date"),
        )
        return self.schema.dump(company)


class CompanyListResource(Resource):
    schema_get = CompanySchema.get_list_schema()
    schema_get_admin = CompanySchema(many=True)
    schema_post = CompanySchema()

    @require_oauth(Scopes.company)
    def get(self):
        is_admin = role_service.user_has_role(current_token.user, Roles.COMPANY_WRITE)

        # If the user is not an administrator, filter the inactive companies.
        if is_admin:
            companies = company_service.find_all_companies(filter_inactive=False)
            return self.schema_get_admin.dump(companies)
        companies = company_service.find_all_companies()
        return self.schema_get.dump(companies)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema_post)
    def post(self, company_details):
        company = company_service.create_company(
            name=company_details.get("name"),
            slug=company_details.get("slug"),
            website=company_details.get("website"),
            contract_start_date=company_details.get("contract_start_date"),
            contract_end_date=company_details.get("contract_end_date"),
        )
        return self.schema_post.dump(company)
