from http import HTTPStatus
from typing import Dict, Any

from flask import Response, request
from flask_restful import Resource
from marshmallow import fields

from app.api.schema import (
    RestSchema,
    AutoMultilangStringField,
    PaginatedResponseSchema,
    PaginatedSearchSchema,
    PageSearchParameters,
)
from app.decorators import (
    json_schema,
    require_oauth,
    require_role,
    query_parameter_schema,
)
from app.models.committee import Committee
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import committee_service


class CommitteeSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    modified = fields.Date(dump_only=True)

    name = AutoMultilangStringField(required=True)
    picture_file_id = fields.Integer(default=None)

    class CoordinatorSchema(RestSchema):
        id = fields.Integer(required=True)
        email = fields.Email(dump_only=True)
        first_name = fields.String(dump_only=True)
        last_name = fields.String(dump_only=True)

    class CommitteeSchema(RestSchema):
        id = fields.Integer(required=True)
        name = fields.String(dump_only=True)

    class PageSchema(RestSchema):
        id = fields.Integer(required=True)
        path = fields.String(dump_only=True)

    coordinator = fields.Nested(CoordinatorSchema())
    group = fields.Nested(CommitteeSchema())
    page = fields.Nested(PageSchema())
    coordinator_interim = fields.Boolean(default=False)
    open_new_members = fields.Boolean(default=False)
    tags = AutoMultilangStringField(required=True)
    pressure = fields.Integer(default=0)

    @classmethod
    def get_list_schema(cls):
        return cls(many=True)


class CommitteeResource(Resource):
    schema = CommitteeSchema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_READ)
    def get(self, committee: Committee):
        return self.schema.dump(committee)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema)
    def put(self, data: Dict[str, Any], committee: Committee):
        committee_service.edit_committee(
            committee,
            data["name"],
            data["coordinator"]["id"],
            data["group"]["id"],
            data["page"]["id"],
            data["coordinator_interim"],
            data["open_new_members"],
            data["tags"],
            data["pressure"],
        )

        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    def delete(self, committee: Committee):
        committee_service.delete_committee(committee)
        return Response(status=HTTPStatus.NO_CONTENT)


class CommitteeListResource(Resource):
    schema_get = PaginatedResponseSchema(CommitteeSchema.get_list_schema())
    schema_search = PaginatedSearchSchema()
    schema_post = CommitteeSchema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @query_parameter_schema(schema_search)
    def get(self, pagination: PageSearchParameters):
        pagination_result = committee_service.paginated_search_all_committees(
            pagination
        )
        return self.schema_get.dump(pagination_result)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema_post)
    def post(self, data: dict):
        committee_created = committee_service.create_committee(
            data["name"],
            data["coordinator"]["id"],
            data["group"]["id"],
            data["page"]["id"],
            data["coordinator_interim"],
            data["open_new_members"],
            data["tags"],
            data["pressure"],
        )
        return self.schema_post.dump(committee_created)


class CommitteePictureResource(Resource):
    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def put(self, committee: Committee):
        if "file" not in request.files:
            return Response(status=HTTPStatus.BAD_REQUEST)

        committee_service.set_committee_picture(committee, request.files["file"])
        return Response(status=HTTPStatus.NO_CONTENT)
