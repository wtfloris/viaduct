#!/bin/bash
set -e

flask db upgrade
exec uwsgi --ini docker/uwsgi.ini
