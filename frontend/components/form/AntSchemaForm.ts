import { Component, Vue } from "vue-property-decorator";
import { WrappedFormUtils } from "ant-design-vue/types/form/form";
import { Button, Checkbox, Form, Input, Select } from "ant-design-vue";
import { CreateElement, VNode, VNodeDirective } from "vue";
import instance from "../../utils/axios";
import Flask from "../../utils/flask";
import { validateAntForm } from "../../utils/form";

export interface AntSchemaForm {
    schemaEndpoint: string;
    save(r: unknown): Promise<void>;
}

@Component({
    components: {
        "a-form": Form,
        "a-form-item": Form.Item,
        "a-input": Input,
        "a-button": Button,
        "a-select": Select,
        "a-checkbox": Checkbox,
        "a-select-option": Select.Option,
        "a-textarea": Input.TextArea,
    },
})
export class AntSchemaForm extends Vue {
    form: WrappedFormUtils | null = null;
    // TODO write custom JSONSchema types instead of any
    //  The https://www.npmjs.com/package/openapi-types does not seem to work
    //  nice:
    //  - JSONSchema has no default
    //  - undefined all over the place where things are actually guaranteed
    //    by our backend.
    schema: any = {};
    endpointUrl = "Set this in children";
    loading = false;

    async created(): Promise<void> {
        this.schema = (
            await instance.get(Flask.url_for(this.schemaEndpoint))
        ).data;
        this.form = this.$form.createForm(this);
        this.$i18n.mergeLocaleMessage("nl", {
            required: "{field} is required",
        });
        this.$i18n.mergeLocaleMessage("en", {
            required: "{field} is vereist",
        });
    }

    render(createElement: CreateElement): VNode {
        if (!this.schema.properties) {
            return createElement("div");
        }

        const children: VNode[] = Object.keys(
            this.schema.properties
        ).map((propKey) => this.renderFormItem(createElement, propKey));

        // submit button
        children.push(
            createElement("a-form-item", [
                createElement(
                    "a-button",
                    {
                        props: {
                            loading: this.loading,
                        },
                        attrs: {
                            type: "primary",
                            htmlType: "submit",
                        },
                    },
                    [this.$t("Submit") as string]
                ),
            ])
        );

        return createElement("div", [
            createElement(
                "a-form",
                {
                    props: {
                        form: this.form,
                    },

                    on: {
                        submit: async (e: Event): Promise<void> => {
                            this.loading = true;
                            e.preventDefault();
                            let values;
                            try {
                                values = await validateAntForm(this.form!);
                            } catch (e) {
                                return;
                            }
                            await this.save(values);
                            this.loading = false; // Form didn't validate
                        },
                    },
                },
                children
            ),
        ]);
    }

    renderFormItem(createElement: CreateElement, propKey: string): VNode {
        if (this.schema.properties[propKey]["$ref"]) {
            throw new Error(
                "Nested schemas are not yet supported in AntSchemaForm."
            );
        }
        let child: VNode;
        switch (this.schema.properties[propKey].type) {
            case "string":
                child = this.renderFormInput(createElement, propKey);
                break;
            case "boolean":
                child = this.renderFormBool(createElement, propKey);
                break;
            case "number":
            case "integer":
            case "object":
            case "array":
            default:
                throw new Error(
                    `Type ${this.schema.properties[propKey].type} has not yet been implemented in AntSchemaForm.`
                );
        }

        return createElement(
            "a-form-item",
            {
                props: {
                    label: this.renderFormItemLabel(propKey),
                    labelCol: {
                        span: 24,
                        lg: { span: 4 },
                    },
                    wrapperCol: {
                        span: 24,
                        lg: { span: 20 },
                    },
                },
            },
            [child]
        );
    }

    renderFormItemLabel(propKey: string): string {
        return this.$t(`form.${propKey}.label`) as string;
    }

    renderFormInput(createElement: CreateElement, propKey: string): VNode {
        if (this.schema.properties[propKey]?.enum) {
            return this.renderFormSelect(createElement, propKey);
        }

        let el;
        switch (this.schema.properties[propKey]?.format) {
            case "textarea":
                el = "a-textarea";
                break;
            default:
                el = "a-input";
        }
        return createElement(el, {
            attrs: { placeholder: this.renderFormItemLabel(propKey) },
            directives: [this.renderDecoratorDirective(propKey)],
        });
    }

    renderFormSelect(createElement: CreateElement, propKey: string): VNode {
        const children: VNode[] = this.schema.properties[propKey].enum.map(
            (v) => {
                return createElement(
                    "a-select-option",
                    { attrs: { value: v } },
                    [this.$t(`form.${propKey}.value.${v}`) as string]
                );
            }
        );

        return createElement(
            "a-select",
            {
                directives: [this.renderDecoratorDirective(propKey)],
            },
            children
        );
    }

    renderDecoratorDirective(propKey: string): VNodeDirective {
        let required = {};
        if (this.schema.required?.includes(propKey))
            required = {
                required: true,
                message: this.$t(`required`, {
                    field: this.renderFormItemLabel(propKey),
                }),
            };

        let initialValue = {};
        if (this.schema.properties[propKey].default) {
            initialValue = {
                initialValue: this.schema.properties[propKey].default,
            };
        }
        return {
            name: "decorator",
            value: [propKey, { ...initialValue, rules: [required] }],
        };
    }

    renderFormBool(createElement: CreateElement, propKey: string): VNode {
        return createElement("a-checkbox", {
            directives: [this.renderDecoratorDirective(propKey)],
        });
    }
}
