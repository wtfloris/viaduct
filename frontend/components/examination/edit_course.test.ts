import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import EditCourse from "./edit_course.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

const consoleSpy = jest.spyOn(console, "error");

describe(EditCourse.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditCourse, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });
        await flushPromises();

        getByText("Edit course");
    });
});
