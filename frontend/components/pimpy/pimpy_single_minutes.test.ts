import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import { date } from "../../scripts/formatters";
import VueI18n from "vue-i18n";
import PimpySingleMinute from "./pimpy_single_minute.vue";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);
localVue.filter("date", date);
import { i18n } from "../../translations";

const consoleSpy = jest.spyOn(console, "error");

describe(PimpySingleMinute.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PimpySingleMinute, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            props: { userGroups: [] },
        });

        getByText("Minute");
    });
});
