<template lang="pug">
.document-entry
    h5(v-if="!document.id") {{ $t('New document') }}
    a-form(:form="documentForm", @submit.prevent="save")
        a-form-item(v-bind="formItemLayout", :label="$t('Dutch name')")
            a-input(
                v-decorator="['nl_name', {initialValue: document.name.nl, rules: [{ required: true, message: $t('Please enter a Dutch name') }]}]"
            )
        a-form-item(v-bind="formItemLayout", :label="$t('English name')")
            a-input(
                v-decorator="['en_name', {initialValue: document.name.en, rules: [{ required: true, message: $t('Please enter an English name') }]}]"
            )

        a-form-item(
            v-if="document.id",
            v-bind="formItemLayout",
            :label="$t('Versions') + ' (' + document.versions + ')'"
        )
            a-button-group
                a-dropdown-button
                    a(:href="alvDocumentUrl(document.id)") {{ $t('Latest') }}
                    a-menu(slot="overlay")
                        a-menu-item(
                            v-for="n in rangeReverse(document.versions)",
                            :key="n + ''"
                        )
                            a(:href="alvDocumentUrl(document.id, n)")
                                | {{ $t('Version') + ' ' + n }}

        a-form-item(v-bind="formItemLayout", :label="$t('New version')")
            a-upload(
                :multiple="false",
                :fileList="fileList",
                :beforeUpload="() => false",
                @change="(e) => handleUploadChange(e)",
                v-decorator="['file', {rules: [{required: !document.id, message: $t('Please upload a file.')}]}]"
            )
                a-button
                    a-icon(type="upload")
                    | {{ $t('Click or drag to upload') }}
                span.button-hint(v-if="document.id")
                    | {{ $t('Leave this empty to keep the current version.') }}

        a-form-item(
            :wrapperCol="{ ...formItemLayout.wrapperCol, ...{ xs: { offset: 0 }, md: { offset: 4 } } }"
        )
            a-button-group
                a-button(
                    type="primary",
                    html-type="submit",
                    :loading="submitting"
                ) {{ $t('Save') }}
                a-button(v-if="!document.id", @click="$emit('cancel-clicked')") {{ $t('Cancel') }}
</template>

<style lang="sass" scoped>
.document-entry
    border: 1px solid #e8e8e8
    margin: 1em 0
    padding: 1em 1em 0 1em

.document-empty
    border: 1px dashed #e8e8e8
    padding: 1em

.button-hint
    margin-left: 1em
    color: gray
</style>

<script lang="ts">
import { Component, Prop, Vue, Emit } from "vue-property-decorator";
import Flask from "../../utils/flask";
import { alvApi, AlvDocument } from "../../scripts/api/alv";
import {
    Button,
    Upload,
    Form,
    Input,
    Menu,
    Dropdown,
    Icon,
} from "ant-design-vue";
import LoadingContainer from "../loading_container.vue";
import EditAlvEditMinutes from "./edit_alv_minutes.vue";
import { WrappedFormUtils } from "ant-design-vue/types/form/form";
import { validateAntForm } from "../../utils/form";

@Component({
    components: {
        "a-button": Button,
        "a-button-group": Button.Group,
        "a-upload": Upload,
        "a-form": Form,
        "a-form-item": Form.Item,
        "a-input": Input,
        "a-menu": Menu,
        "a-menu-item": Menu.Item,
        "a-dropdown-button": Dropdown.Button,
        "a-icon": Icon,
        "loading-container": LoadingContainer,
        "edit-alv-edit-minutes": EditAlvEditMinutes,
    },
})
export default class EditAlvDocument extends Vue {
    @Prop({
        default: () => {
            return {
                name: {
                    nl: "",
                    en: "",
                },
                versions: 0,
            };
        },
    })
    document: AlvDocument;
    @Prop() alvId?: number;

    formItemLayout = {
        labelCol: {
            span: 24,
            lg: { span: 4 },
        },
        wrapperCol: {
            span: 24,
            lg: { span: 20 },
        },
    };

    submitting = false;
    documentForm?: WrappedFormUtils;
    fileList: File[] = [];

    created() {
        if (!this.document && !this.alvId) {
            throw Error("Need either 'document' or 'alvId' prop.");
        }

        this.documentForm = this.$form.createForm(this);
    }

    async updateDocument(values, docId: number) {
        try {
            await alvApi.setAlvDocumentMetaData(
                docId,
                values.nl_name,
                values.en_name
            );

            if (values.file) {
                const formData = new FormData();
                formData.append("file", this.fileList[0]);

                await alvApi.uploadAlvDocument(docId, formData);
                this.document!.versions += 1;
            }

            this.$notification.success({
                message: "Success",
                description: this.$t("Successfully saved document.") as string,
            });
        } finally {
            this.submitting = false;
        }
    }

    @Emit("document-created")
    async createDocument(values) {
        let document: AlvDocument | null = null;
        try {
            const resp = await alvApi.createAlvDocument(
                this.alvId!,
                this.fileList[0],
                values.nl_name,
                values.en_name
            );
            document = resp.data;

            this.document.id = document.id;
            this.document.versions = document.versions;

            this.fileList = [];
            this.documentForm!.resetFields(["file"]);

            this.$notification.success({
                message: "Success",
                description: this.$t(
                    "Successfully created document."
                ) as string,
            });
            return document;
        } finally {
            this.submitting = false;
        }
    }

    async save() {
        let values;
        try {
            values = await validateAntForm(this.documentForm!);
        } catch (e) {
            return; // Form didn't validate
        }

        this.submitting = true;

        if (this.document?.id) {
            await this.updateDocument(values, this.document!.id!);
        } else {
            await this.createDocument(values);
        }
    }

    async handleUploadChange(ev) {
        // Make sure only one file is shown
        if (ev.file) {
            this.fileList[0] = ev.file;
        }
    }

    private alvDocumentUrl(docId: number, version?: number) {
        if (!version) {
            return this.alvDocumentUrlLatest(docId);
        }

        return Flask.url_for("alv.view_document_version", {
            alv_document_id: docId,
            version: version,
        });
    }

    private alvDocumentUrlLatest(docId: number) {
        return Flask.url_for("alv.view_document_version_latest", {
            alv_document_id: docId,
        });
    }

    private rangeReverse(n: number) {
        const arr: number[] = [];
        for (let i = n; i > 0; i--) {
            arr.push(i);
        }

        return arr;
    }
}
</script>

<i18n src="./alv.json"></i18n>
