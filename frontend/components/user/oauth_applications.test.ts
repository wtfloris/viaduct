import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import UserOAuthApplications from "./oauth_applications.vue";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/oauth.ts");
const localVue = createLocalVue();
localVue.use(VueI18n);
import { i18n } from "../../translations";
import flushPromises from "flush-promises";

const consoleSpy = jest.spyOn(console, "error");

describe(UserOAuthApplications.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(UserOAuthApplications, {
            localVue: localVue,
            i18n: i18n,
            mocks: {
                $route: {
                    params: {},
                },
            },
        });
        await flushPromises();
        getByText("Connected applications");
    });
});
