import "bootstrap";
import Vue from "vue";
import VueRouter from "vue-router";
import VueCarousel from "vue-carousel";
/*
 * If you want to start using dependencies, please do it the following way:
 * https://bootstrap-vue.js.org/docs/#tree-shaking-with-module-bundlers
 * ~ Wilco - 09-11-2019
 */
import * as Sentry from "@sentry/browser";
import { i18n } from "./translations";
import { Form, notification } from "ant-design-vue";
import "./assets/scss/ant.less";
import { date, datetime } from "./scripts/formatters";
import { router } from "./router";
import { errorHandler } from "./utils/errors";

Vue.use(Form);
Vue.prototype.$notification = notification;

/*
 * These are the "misc" imports, they aren't packed together.
 */
const App = () => import("./components/App.vue");
const FooterCarousel = () => import("./components/footer_carousel.vue");
const WtformDatePicker = () =>
    import("./components/form/wtform_date_picker.vue");
const WtformSelectField = () =>
    import("./components/form/wtform_select_field.vue");

declare let SentryConfig: Sentry.BrowserOptions;
declare let viaduct;

Vue.use(VueRouter);
Vue.use(VueCarousel);
Vue.config.errorHandler = errorHandler;

SentryConfig.integrations = [new Sentry.Integrations.Vue()];
Sentry.init(SentryConfig);

Vue.prototype.$viaduct = viaduct;

// Needed to make sure that 'Sentry' object in globally visible in browser
window["Sentry"] = Sentry;

Vue.filter("date", date);

Vue.filter("datetime", datetime);

Vue.filter("euro", function (value: number) {
    const formatter = new Intl.NumberFormat("nl-NL", {
        style: "currency",
        currency: "EUR",
        minimumFractionDigits: 2,
    });
    return formatter.format(value);
});

Vue.filter("capitalize", function (value: string) {
    //  This filter capitalizes the word. (i.e. 'via' becomes 'Via')
    if (value.length <= 1) return value;
    return value.charAt(0).toUpperCase() + value.slice(1);
});

new Vue({
    el: "#container-main",
    i18n,
    router,
    components: {
        App: App,
        "footer-carousel": FooterCarousel,
        "wtform-date-picker": WtformDatePicker,
        "wtform-select-field": WtformSelectField,
    },
});
