import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";
import { Education } from "../../types/examination";

class EducationApi extends Api {
    getUserEducations(userId: number | null = null): AxiosPromise<Education[]> {
        return this.get<Education[]>(
            Flask.url_for("api.user.educations", {
                user: userId ? userId : "self",
            })
        );
    }
}

export const educationApi = new EducationApi();
