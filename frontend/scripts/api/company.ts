import { Api, PaginatedResponse } from "./api";
import { AxiosResponse } from "axios";
import {
    Company,
    CompanyBanner,
    CompanyJob,
    CompanyJobs,
    CompanyProfile,
    ContractOfServiceTypes,
} from "../../types/company";
import Flask from "../../utils/flask";

class CompanyApi extends Api {
    public async getCompany(
        companyId: number
    ): Promise<AxiosResponse<Company>> {
        return this.get(
            Flask.url_for("api.company", { company_id: companyId })
        );
    }

    public async getCompanyBanner(
        companyId: number
    ): Promise<AxiosResponse<CompanyBanner>> {
        return this.get(
            Flask.url_for("api.company_banner", { company_id: companyId })
        );
    }

    public async getCompanyProfile(
        companyId: number
    ): Promise<AxiosResponse<CompanyProfile>> {
        return this.get(
            Flask.url_for("api.company_profile", { company_id: companyId })
        );
    }

    public async getCompanyJob(
        jobId: number
    ): Promise<AxiosResponse<CompanyJob>> {
        return this.get(Flask.url_for("api.job", { job_id: jobId }));
    }

    public async getContractOfServiceTypes(): Promise<
        AxiosResponse<ContractOfServiceTypes>
    > {
        return this.get(Flask.url_for("api.jobs.types"));
    }

    public async getCompanyJobs(
        search,
        contract_of_service,
        page
    ): Promise<AxiosResponse<PaginatedResponse<CompanyJobs>>> {
        return this.get(
            Flask.url_for("api.jobs", {
                search: search,
                contract_of_service: contract_of_service,
                page: page,
            })
        );
    }
}

export const companyApi = new CompanyApi();
