import en from "./en.json";
import nl from "./nl.json";
import VueI18n from "vue-i18n";
import Vue from "vue";

Vue.use(VueI18n);

export const i18n = new VueI18n({
    locale: window.viaduct?.locale || "en",
    fallbackLocale: "en",
    messages: {
        en: en,
        nl: nl,
    },
    silentFallbackWarn: true,
});
