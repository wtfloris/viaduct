export enum AlvEvent {
    Decision = "DECISION",
    Task = "TASK",
    Section = "SECTION",
    SubSection = "SUBSECTION",
    Timestamp = "TIMESTAMP",
    Vote = "VOTE",
    ParseWarning = "PARSE_WARNING",
}

export type AlvMetaData = {
    chairman?: string;
    secretary?: string;
    start_time?: string;
    stop_time?: string;
    location?: string;

    present: string[];
    proxies: [string, string][];
};

export type AlvEventData = {
    event_type: AlvEvent;
    data: any;
};

export type MinutesAccepted = "accepted" | "not_accepted" | "unknown";
