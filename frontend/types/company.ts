export enum CompanyModuleType {
    None = "None",
    Company = "Company contract",
    CompanyJob = "Job",
    CompanyBanner = "Banner",
    CompanyProfile = "Profile",
}

export class Company {
    public id: number;
    public name = "";
    public website = "";
    public slug = "";
    public active = false;
    public contract_start_date = "";
    public contract_end_date = "";
    public logo = false;
}

export class CompanyJobBase {
    public id: number;
    public company_id: number;
    public company_name: number;

    public title_nl = "";
    public title_en = "";
    public contract_of_service = "";

    public description_nl = "";
    public description_en = "";
}

export class CompanyJob extends CompanyJobBase {
    public company_id: number;

    public contact_name = "";
    public contact_email = "";
    public contact_address = "";
    public contact_city = "";

    public website = "";
    public phone = "";

    public description_nl = "";
    public description_en = "";

    // ModuleMixin
    public start_date = "";
    public end_date = "";
    public enabled = true;
}

export class CompanyBannerBase {
    public company_id: number;
    public website = "";
}

export class CompanyBanner extends CompanyBannerBase {
    public id;
    public created = "";
    public active = false;

    // ModuleMixin
    public enabled = true;
    public start_date = "";
    public end_date = "";
}

export class CompanyProfile {
    public id;
    public created = "";
    readonly active: boolean = false;

    public description_nl = "";
    public description_en = "";

    // ModuleMixin
    public enabled = true;
    public start_date = "";
    public end_date = "";
}

export class ContractOfServiceTypes {
    public contract_of_service: string[] = [];
}

export class CompanyJobs {
    company_id: number;
    company_name: string;
    contract_of_service: ContractOfServiceTypes;
    company_jobs: CompanyJobBase[];
}
