import { userApi } from "../scripts/api/user";
import { RolesResponse } from "../types/role";
import { getCookie } from "../utils/axios";

class AuthStore {
    roles: Partial<RolesResponse> = {};

    async reloadRoles() {
        if (!this.isAnonymous()) {
            this.roles = (await userApi.getRoles(null)).data;
        }
    }

    isAnonymous() {
        return !getCookie("access_token");
    }
}

export const authStore = new AuthStore();
