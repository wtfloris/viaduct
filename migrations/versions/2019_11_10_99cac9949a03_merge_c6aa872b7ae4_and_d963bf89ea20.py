"""Merge c6aa872b7ae4 and d963bf89ea20.

Revision ID: 99cac9949a03
Revises: ('d963bf89ea20', 'c6aa872b7ae4')
Create Date: 2019-11-10 14:01:12.013035

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "99cac9949a03"
down_revision = ("d963bf89ea20", "c6aa872b7ae4")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    pass


def downgrade():
    create_session()

    pass


# vim: ft=python
