"""Make exam file nullable, to allow API upload'.

Revision ID: 558a94f0560b
Revises: e1e37ca6d57f
Create Date: 2020-10-12 15:35:31.397198

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "558a94f0560b"
down_revision = "e1e37ca6d57f"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "examination", "examination_file_id", existing_type=sa.INTEGER(), nullable=True
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        "examination", "examination_file_id", existing_type=sa.INTEGER(), nullable=False
    )
    # ### end Alembic commands ###


# vim: ft=python
