"""Convert committee_revision to committee table..

Revision ID: 085bcb5a2da2
Revises: 55e35001fc86
Create Date: 2020-11-01 11:42:50.465005

"""
import logging
from datetime import datetime

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "085bcb5a2da2"
down_revision = "1e3ca8680c96"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)


class Group(db.Model):
    __tablename__ = "group"

    id = db.Column(db.Integer, primary_key=True)


class File(db.Model):
    __tablename__ = "file"

    id = db.Column(db.Integer, primary_key=True)


class Page(db.Model):
    __tablename__ = "page"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), default=datetime.now)
    modified = db.Column(db.DateTime(timezone=True), default=datetime.now)
    path = db.Column(db.String(200), unique=True)
    needs_paid = db.Column(db.Boolean)
    deleted = db.Column(db.DateTime(timezone=True), nullable=True)
    type = db.Column(db.String(256))


class Committee(db.Model):
    __tablename__ = "committee"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), default=datetime.now)
    modified = db.Column(db.DateTime(timezone=True), default=datetime.now)

    nl_name = db.Column(db.String(256))
    en_name = db.Column(db.String(256))

    page_id = db.Column(db.Integer, db.ForeignKey("page.id"))

    group_id = db.Column(db.Integer, db.ForeignKey("group.id"))
    coordinator_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    picture_file_id = db.Column(db.Integer, db.ForeignKey("file.id"))

    coordinator_interim = db.Column(db.Boolean)
    open_new_members = db.Column(db.Boolean, nullable=False, default=0)
    en_tags = db.Column(db.String(256), nullable=False, default="")
    nl_tags = db.Column(db.String(256), nullable=False, default="")
    pressure = db.Column(db.Integer, nullable=False, default=0)


class CommitteeRevision(db.Model):
    __tablename__ = "committee_revision"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), default=datetime.now)
    modified = db.Column(db.DateTime(timezone=True), default=datetime.now)

    nl_title = db.Column(db.String(128))
    en_title = db.Column(db.String(128))
    comment = db.Column(db.String(1024))

    nl_description = db.Column(db.Text)
    en_description = db.Column(db.Text)
    group_id = db.Column(db.Integer, db.ForeignKey("group.id"))
    coordinator_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    interim = db.Column(db.Boolean)
    open_new_members = db.Column(db.Boolean, nullable=False, default=0)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    page_id = db.Column(db.Integer, db.ForeignKey("page.id"))


class PageRevision(db.Model):
    __tablename__ = "page_revision"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), default=datetime.now)
    modified = db.Column(db.DateTime(timezone=True), default=datetime.now)

    nl_title = db.Column(db.String(128))
    en_title = db.Column(db.String(128))
    comment = db.Column(db.String(1024))

    nl_content = db.Column(db.Text)
    en_content = db.Column(db.Text)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    page_id = db.Column(db.Integer, db.ForeignKey("page.id"))


def upgrade():
    create_session()

    # add COMMITTEE_PICTURE to the file category type.
    # note: this requires the use of a separate transaction, as ALTER TYPE
    # cannot be used in a transaction.
    # The downgrade does not remote the type as this is getting very
    # complicated as such we check if it does not exists yet.
    with op.get_context().autocommit_block():
        op.execute(
            "ALTER TYPE file_category ADD VALUE IF NOT EXISTS 'COMMITTEE_PICTURE';"
        )

    op.create_table(
        "committee",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created", sa.DateTime(timezone=True), nullable=True),
        sa.Column("modified", sa.DateTime(timezone=True), nullable=True),
        sa.Column("en_name", sa.String(length=256), nullable=False),
        sa.Column("nl_name", sa.String(length=256), nullable=False),
        sa.Column("page_id", sa.Integer(), nullable=False),
        sa.Column("group_id", sa.Integer(), nullable=False),
        sa.Column("coordinator_id", sa.Integer(), nullable=True),
        sa.Column("picture_file_id", sa.Integer(), nullable=True),
        sa.Column("coordinator_interim", sa.Boolean(), nullable=True),
        sa.Column("open_new_members", sa.Boolean(), nullable=False),
        sa.Column("en_tags", sa.String(length=256), nullable=False),
        sa.Column("nl_tags", sa.String(length=256), nullable=False),
        sa.Column("pressure", sa.Integer(), nullable=False, default=0),
        sa.ForeignKeyConstraint(
            ["coordinator_id"],
            ["user.id"],
            name=op.f("fk_committee_coordinator_id_user"),
        ),
        sa.ForeignKeyConstraint(
            ["page_id"], ["page.id"], name=op.f("fk_committee_page_id_page")
        ),
        sa.ForeignKeyConstraint(
            ["group_id"], ["group.id"], name=op.f("fk_committee_group_id_group")
        ),
        sa.ForeignKeyConstraint(
            ["picture_file_id"],
            ["file.id"],
            name=op.f("fk_committee_picture_file_id_file"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_committee")),
        sqlite_autoincrement=True,
    )

    # Create new Committee models from the latest committee page revisions
    for committee_page in (
        db.session.query(Page)
        .filter(Page.type == "committee", Page.deleted.is_(None))
        .all()
    ):
        latest_rev = (
            db.session.query(CommitteeRevision)
            .filter(CommitteeRevision.page_id == committee_page.id)
            .order_by(CommitteeRevision.id.desc())
            .first()
        )

        committee = Committee(
            created=committee_page.created,
            modified=committee_page.modified,
            nl_name=latest_rev.nl_title,
            en_name=latest_rev.en_title,
            page_id=committee_page.id,
            group_id=latest_rev.group_id,
            coordinator_id=latest_rev.coordinator_id,
            coordinator_interim=latest_rev.interim,
            open_new_members=latest_rev.open_new_members,
        )
        db.session.add(committee)

    # Convert all committee revisions to page revisions
    for committee_revision in db.session.query(CommitteeRevision).order_by(
        CommitteeRevision.id.asc()
    ):
        page_revision = PageRevision(
            created=committee_revision.created,
            modified=committee_revision.modified,
            nl_title=committee_revision.nl_title,
            en_title=committee_revision.en_title,
            nl_content=committee_revision.nl_description,
            en_content=committee_revision.en_description,
            comment=committee_revision.comment,
            user_id=committee_revision.user_id,
            page_id=committee_revision.page_id,
        )
        db.session.add(page_revision)

    db.session.commit()

    # op.drop_table("committee_revision")


def downgrade():
    create_session()

    logging.warning(
        "Cannot revert this revision properly as the revisions were merged in the upgrade process."
        "This downgrade will only delete the committee table."
    )

    op.drop_table("committee")


# vim: ft=python
