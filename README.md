# Viaduct
Viaduct is the codebase for the website of [study association via](https://svia.nl).


## Docker setup
**Note: if you are setting up the environment for the first time, please update any errors in the documentation immediately**

The most straightforward and preferred method for getting started developing Viaduct is using Docker. Please follow the steps below.

### i) Install Docker and Docker-Compose
Docker and Docker-Compose are both prerequisites for running Viaduct.

Start by installing Docker CE (Community Edition) by going [here](https://docs.docker.com/install/) and selecting your
 operating system in the menu on the left.

To install docker-compose, please follow the instructions [here](https://docs.docker.com/compose/install/).

### ii) Configure database

Configuring the database consists of two steps.

First, setup Postgres with a viaduct user using Docker-Compose.

```bash
docker-compose up -d db adminer
```

Second, initialize the database structure and fill the database with some basic data using the Docker-Compose command below.
You will be prompted with creating a personal admin account for your local environment.
This will also load all the required configurations needed for running Viaduct.

```bash
docker-compose run --rm backend flask createdb
```

To verify that the database has been setup correctly, go to `adminer` at.

```
http://localhost:8201
```

Here, login using the following (the Database field should be empty):
```
System: PostgreSQL
Server: db
Username: viaduct
Password: viaduct
Database:
```

Now verify that the database `viaduct` exists and contains data.

### iii) Build the site
In order to install all the Docker images and packages required for Viaduct, run the following command.

```bash
docker-compose build backend
```

### iv) (optional) Start background worker

The site uses a worker for background tasks, in order to start it you need to start the broker redis:

```bash
docker-compose up -d redis
```

After that the worker can be started using:

```bash
docker-compose up worker
```

### v) Run the site using Docker

When the database and backend has been setup correctly, the backend of the website can be started locally
by simply running.

```bash
docker-compose up backend
```

Some pages use Vue and thus Webpack also needs to be started. This can be done as follows.

1) While the backend is running, open a new terminal and run.

```
docker-compose exec backend bash
```

2) This will open bash in the backend container, now run

```
./watch.sh assets
```

Webpack will now build the Vue files of the site and watch for changes. After this, you can
load your front-end changes by doing a hard-refresh in the browser. Webpack will reload the
javascript and css of the site.


### vi) Testing code style and committing

We use [Pre-commit](https://pre-commit.com/) for running hooks upon commit. Install it using
the described methods in documentation. Afterwards you need to initialize it using:

```bash
pre-commit install
```

Upon commit, all the hooks will run. Some hooks are configured to automatically
fix mistakes you have made, some will require manual intervention.
A manual check without making a commit can be done by running:

```bash
pre-commit run --all-files
```

### v) Run viaduct from PyCharm (bonus)
*These instructions should translate to Intelij Ultimate*

It is possible to easily add the Docker Python interpreter directly to PyCharm, which allows you to
i) use PyCharm to check and autocomplete your imports, and ii) run Viaduct in the PyCharm debugger, so you can insert breakpoints on the fly.

#### Add the Docker Python interpreter.

1) Open the PyCharm settings (ctrl+alt+s) and navigate to the `project interpreter` settings.
2) Add a new interpreter by pressing gear icon and then the `add` as shown below ![alt text](docs/readme/pycharm-add-interpreter.png)
3) Click on the `Docker Compose` tab and add an interpreter with the following settings. If no Server exists, then add a Docker server with the default settings. ![alt text](docs/readme/pycharm-python-interpreter.png "Logo Title Text 1")
4) After pressing `OK`, a list with all packages viaduct pip should show. Press `Apply` or `OK` to save the settings.

#### Create a run configuration
*A run configuration called `docker-viaduct-backend` should already exist. Follow these steps if that is not the case.*

1) Open the run configuration (shift+alt+f10, then 0).
2) Open the template for python on the left and create a configuration with the following options:
    1) Module Name: `flask`
    2) Parameters: `run --host=0.0.0.0`
    3) Environment Variables: `PYTHONUNBUFFERED=1;FLASK_ENV=development`
    4) Python Interpreter: Use the one you created in the previous step.
3) Save the configuration by pressing `OK` or `Apply`.
4) After running this once, a run configuration should be available in the top right corner of PyCharm.

#### Run / Debug Viaduct

In the top right of PyCharm, the following should be visible (`docker-viaduct-backend` could also be called `run`).
To start running Viaduct press the green triangle (or shift+alt+f10), for debugging press the bug icon.

![alt text](docs/readme/pycharm-run-debug.png)

To learn more about how to optimize your development flow, read about the PyCharm debugger [here](https://www.jetbrains.com/help/pycharm/debugging-your-first-python-application.html).

## Running pytest

We use [pytest](https://docs.pytest.org/en/latest/) for running our test-suite. It requires a database to create run the integration tests.
You are able to use your development postgresql connection for testing, however that will wipe all data after the test run.
A better solution is to use a specific database for the tests. To create this database in your development postgresql instance, follow the following steps:

```bash
[user@local ~/IdeaProjects/viaduct]$ docker-compose exec db bash  # to drop into the database container.
bash-4.4# psql -U viaduct                                         # to drop into the postgres shell.
psql (10.6)
Type "help" for help.

viaduct=# create database viaduct_test;                           # to create a new database named "viaduct_test".
```

You can now use the `docker-pytest` task from within PyCharm. If you manually want to run the test suite, you can execute pytest within the backend container using the following command:
```bash
docker-compose exec backend pytest
```


## Manual Setup (not recommended)

Please start with following step i) and ii) in the Docker setup for setting up the database.

### Python environment

For the Python dependencies, usage of virtual environments is recommended.
All dependencies are listed in `pyproject.toml`.
Using `poetry` we compile a list of the dependencies with pinned versions,
which are contained in `poetry.lock`. See [Poetry](https://python-poetry.org/)
for documentation.
To install the dependencies in a virtual environment, run the following command:

```bash
poetry install
```

We expose a handful of CLI commands, for example:

* Add an existing user to the administrators group:
    - `flask admin add <your name>`
* Listing all URL routes in the applications:
    - `flask routes`
* And more:
    - `flask --help`

### NodeJS tools

**Note:** Only needed for running outside docker

Build dependencies are npm:
* Install build dependencies and watch files for updates:
    - `npm install`
    - `npm run dev`

## Best practices

### Changes in the database

To make changing the database easy you can use the models to update the actual
database. There are two times you want to do this. The first one is just for
testing your changes locally.
If this is the case use these commands to upgrade your actual database.

This will create a new migration script:

```bash
flask db migrate --message 'revision message'
```

After this script is done you can view it to check if nothing weird is
going to happen when you execute it. After that, just run the server with
docker-compose and it will automatically execute the migration, or run it
manually:

```bash
flask db upgrade
```

If this causes errors, something is wrong. Quite possibly the state of the
database, if you can't fix it yourself ask for help.  If not, you now have an up
to date database.

#### Pushing migrations to develop
When pushing a new migration to develop, two scenarios can occur:

1. No new migrations were pushed to master in the meantime. This means that your
    migration is correctly the new head of database changes. You can verify this
    by running `flask db history` and check if your change is at the top, and
    the succeeding versions follow another correctly.

2. Other changes were made to the database in the meantime. This means that the
    current history is incorrect and needs to be fixed. See the section below to
    find out how.

#### Fixing migration history
Every migration has its unique `revision` hash, which is noted at the beginning
of the migration file. Note that a `down_revision` hash is also provided here.
We use `Alembic` to figure out the correct order of revisions. The current order
can be shown via `flask db history`. The current head revision is noted at the
top. In order to append your new migration to the history, change the
`down_revision` hash of your migration file to the hash of the current head.
Then check if all versions follow another correctly in the history of
`flask db history`.

*If you have any questions when merging, do ask for help!*

### Language

tl;dr For compiling the strings we have a PyCharm task that will run these
commands in sequence Just run `docker-translations` in PyCharm them.

All code shall be written in **English**, translations should be added through
Babel. After writing code with **English** strings, add their **Dutch**
translations.

Creating strings in python is done using the `(lazy_)gettext` functions.

```python
from flask_babel import _  # in views
from flask_babel import lazy_gettext as _   # in models and forms
```

For updating translation files after creating new pages, first extract the new
translatable strings from the code. Then merge the new extractions with the
existing translations:

```bash
flask translations
```

Edit the file `app/translations/nl/LC_MESSAGES/message.po` and add the Dutch
translations for the English strings. Especially look for lines marked "fuzzy",
since they won't be compiled. If the translation is correct, remove the line
marking "fuzzy" and continue.

After that compile the strings to be used in the website using the same command:

```bash
flask translations
```


### Documentation

Documentation according to Python's [Docstring Conventions]
(http://www.python.org/dev/peps/pep-0257/).


## Troubleshooting:
```
IOError: [Errno 13] Permission denied: '/home/username/.pip/pip.log'
```
This can also happen for other files. It often happens when files are created within a running Docker container. To fix, execute the following: `sudo chown $USER:$USER /home/username/.pip/pip.log`.

```
ERROR: Version in "./docker-compose.yml" is unsupported. You might be seeing
this error because you're using the wrong Compose file version. Either specify
a version of "2" (or "2.0") and place your service definitions under the `services`
key, or omit the `version` key and place your service definitions at the root of
the file to use version 1. For more on the Compose file format versions,
see https://docs.docker.com/compose/compose-file/
```

  Your docker-compose version is not up to date, please update it to a newer version
  following the instructions [here](https://docs.docker.com/compose/install/). Make
  sure to remove your old version first.
