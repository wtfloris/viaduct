import pytest

from app.models.course import EducationCourse


@pytest.fixture
def course_with_education_link(admin_user, course_factory, education_factory, db):
    e1 = education_factory()
    admin_user.educations = [e1]
    c1 = course_factory()
    ec = EducationCourse(education_id=e1.id, course_id=c1.id, year="1", periods=[1])
    db.session.add(ec)
    db.session.commit()
    return {"education": e1, "course": c1, "education_course": ec}


def test_list_courses(admin_client, course_factory):
    course = course_factory()
    rv = admin_client.get("/api/courses/")
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course.id, rv.json


def test_list_courses_by_education(admin_client, course_with_education_link):
    rv = admin_client.get(
        "/api/courses/",
        query_string={
            "user_educations": f"{course_with_education_link['education'].id},1"
        },
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course_with_education_link["course"].id, rv.json


def test_list_courses_by_datanose_id(admin_client, course_with_education_link):
    rv = admin_client.get(
        "/api/courses/",
        query_string={"dn_course_ids": f"{course_with_education_link['course'].id}"},
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["data"][0]["id"] == course_with_education_link["course"].id, rv.json
