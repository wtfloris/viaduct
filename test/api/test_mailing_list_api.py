from typing import List

import pytest

from app.models.mailinglist_model import MailingList
from app.models.user import User
from conftest import CustomClient


@pytest.fixture
def mailinglists(db_session):
    mailinglist1 = MailingList(
        nl_name="Nieuwsbrief",
        en_name="Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    mailinglist2 = MailingList(
        nl_name="Bedrijfsinformatie",
        en_name="Company information",
        copernica_column_name="Bedrijfsinformatie",
        members_only=True,
    )

    db_session.add(mailinglist1)
    db_session.add(mailinglist2)
    db_session.commit()

    return [mailinglist1, mailinglist2]


@pytest.fixture
def subscribed_user(db_session, mailinglists, user_factory):
    user: User = user_factory()
    user.mailinglists = mailinglists

    db_session.commit()

    return user


def test_list_all(admin_client: CustomClient, mailinglists: List[MailingList]):

    rv = admin_client.get("/api/mailinglists/")
    assert rv.status_code == 200

    content = rv.json
    assert len(content) == len(mailinglists)

    for i, ml in enumerate(mailinglists):
        assert content[i]["nl_name"] == ml.nl_name
        assert content[i]["en_name"] == ml.en_name


def test_get_single(admin_client: CustomClient, mailinglists: List[MailingList]):

    ml = mailinglists[0]
    rv = admin_client.get(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 200

    content = rv.json

    assert content["nl_name"] == ml.nl_name
    assert content["en_name"] == ml.en_name


def test_get_single_nonexistent(admin_client: CustomClient):

    rv = admin_client.get("/api/mailinglists/123/")
    assert rv.status_code == 404


def test_create_new(admin_client: CustomClient, db_session):
    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": "some_column_name",
        "members_only": True,
        "default": False,
    }
    rv = admin_client.post("/api/mailinglists/", json=data)
    assert rv.status_code == 201

    ml = db_session.query(MailingList).filter_by(nl_name=data["nl_name"]).one_or_none()

    assert ml is not None
    assert ml.en_name == data["en_name"]
    assert ml.copernica_column_name == data["copernica_column_name"]
    assert ml.members_only == data["members_only"]
    assert ml.default == data["default"]


def test_create_new_invalid_copernica_column_format(admin_client: CustomClient):

    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": "some value with spaces",
        "members_only": True,
        "default": False,
    }

    rv = admin_client.post("/api/mailinglists/", json=data)
    assert rv.status_code == 409


def test_update(
    admin_client: CustomClient, mailinglists: List[MailingList], db_session
):

    ml = mailinglists[0]

    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": ml.copernica_column_name,
        "members_only": ml.members_only,
        "default": ml.default,
    }

    rv = admin_client.put(f"/api/mailinglists/{ml.id}/", json=data)
    assert rv.status_code == 200

    db_session.refresh(ml)
    assert ml.nl_name == data["nl_name"]
    assert ml.en_name == data["en_name"]


def test_delete(
    admin_client: CustomClient, mailinglists: List[MailingList], db_session
):

    ml = mailinglists[0]

    rv = admin_client.delete(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 204

    ml = db_session.query(MailingList).filter_by(id=ml.id).one_or_none()
    assert ml is None


def test_delete_nonexistent(admin_client: CustomClient):

    rv = admin_client.delete("/api/mailinglists/123456/")
    assert rv.status_code == 404


def test_delete_subscribed_users(
    admin_client: CustomClient,
    subscribed_user: User,
    mailinglists: List[MailingList],
    db_session,
):

    ml = mailinglists[0]

    rv = admin_client.delete(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 409

    ml = db_session.query(MailingList).filter_by(id=ml.id).one_or_none()
    assert ml is not None
