from datetime import date


def test_create_edit(admin_client, admin_user, activity_factory):
    activity = activity_factory()

    rv = admin_client.post(
        "/api/alvs",
        json={
            "name": {"nl": "nl_name", "en": "en_name"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
            "secretary_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 201, rv.json

    rv = admin_client.get(f"/api/alvs/{rv.json['id']}")
    assert rv.status_code == 200, rv.json

    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
            "secretary_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 200, rv.json


def test_create_edit_empty(admin_client, admin_user, activity_factory):
    activity = activity_factory()

    rv = admin_client.post(
        "/api/alvs",
        json={
            "name": {"nl": "nl_name", "en": "en_name"},
            "date": date.today().isoformat(),
        },
    )
    assert rv.status_code == 201, rv.json
    assert rv.json["secretary"] is None

    rv = admin_client.get(f"/api/alvs/{rv.json['id']}")
    assert rv.status_code == 200, rv.json

    #  Adds activity and chairman
    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "chairman_user_id": admin_user.id,
        },
    )
    assert rv.status_code == 200, rv.json

    #  Change other field while keeping chairman
    rv = admin_client.patch(
        f"/api/alvs/{rv.json['id']}",
        json={
            "name": {"nl": "nl_name2", "en": "en_name2"},
            "date": date.today().isoformat(),
            "activity_id": activity.id,
            "minutes_accepted": True,
        },
    )
    assert rv.status_code == 200, rv.json
    assert rv.json["chairman"] is not None
    assert rv.json["minutes_accepted"]
