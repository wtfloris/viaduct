from app.models.activity import Activity


def test_activity_list(admin_client, activity):
    rv = admin_client.get("/api/activities")
    assert rv.status_code == 200
    assert rv.json["data"][0]["id"] == activity.id


def test_activity_get(admin_client, activity):
    rv = admin_client.get(f"/api/activities/{activity.id}")
    assert rv.status_code == 200
    assert rv.json["id"] == activity.id


def test_activity_create(db, admin_client):
    rv = admin_client.post(
        "/api/activities",
        json={
            "name": {"en": "asdf", "nl": "asdf"},
            "description": {"en": "adsf", "nl": "asdf"},
            "price": "1",
            "location": "Studievereniging VIA, Science Park 904, Amsterdam",
            "start_time": "2020-02-18T13:35:51.436Z",
            "end_time": "2020-02-18T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )
    assert rv.status_code == 200
    activity = db.session.query(Activity).one_or_none()
    assert activity
    assert activity.name
