import pytest

urls = [
    "/challenge/",
    "/challenge/dashboard/",
    "/companies/admin/",
    "/courses/",
    "/courses/create",
    "/education",
    "/examination",
    "/groups/create",
    "/pages/",
    "/tutoring",
    "/tutoring/request",
    "/jobs/",
    "/companies/",
    "/photos/",
    "/photos/123456789",
    "/search/",
    "/pimpy/",
    "/pimpy/tasks/",
    "/pimpy/tasks/create/",
    "/pimpy/minutes/",
    "/pimpy/minutes/create/",
    "/redirect/",
    "/bug/report/",
]


@pytest.mark.parametrize("url", urls)
def test_vue_pages_without_requirements(url, anonymous_client, admin_user):
    anonymous_client.login(admin_user)
    rv = anonymous_client.get(url)
    assert rv.status_code == 200
