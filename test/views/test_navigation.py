from typing import List

import pytest

from app.models.navigation import NavigationEntry
from app.models.page import Page, PageRevision
from conftest import CustomClient


@pytest.fixture
def page_long_url_revision(db_session, admin_user):
    page = Page(path="activities/archive/someurl", type="page")

    revision = PageRevision(
        page=page,
        nl_title="nl_title",
        en_title="en_title",
        comment="comment",
        user=admin_user,
        nl_content="nl_content",
        en_content="en_content",
    )
    db_session.add(page)
    db_session.add(revision)
    db_session.commit()
    return page, revision


def test_navigation(
    anonymous_client: CustomClient,
    admin_user,
    db_session,
    sample_navigation: List[NavigationEntry],
    page_long_url_revision,
):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/")
    assert rv.status_code == 200

    data = rv.data.decode()
    for entry in sample_navigation:
        assert entry.en_title in data

    rv = anonymous_client.get("/path")
    assert rv.status_code == 200

    rv = anonymous_client.get("/activities/archive/someurl")
    assert rv.status_code == 200

    rv = anonymous_client.get("/activities/archive/")
    assert rv.status_code == 200


def test_navigation_vue_pages(anonymous_client, admin_user, sample_navigation):
    anonymous_client.login(admin_user)
    rv = anonymous_client.get("/navigation/create")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/navigation/{sample_navigation[0].id}/edit/")
    assert rv.status_code == 200
