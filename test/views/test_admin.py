import pytest

urls = [
    "/admin/",
    "/admin/async/",
    "/redirect/",
    "/groups/",
    "/navigation/",
    "/companies/admin/",
    "/examination/",
    "/education/",
    "/courses/",
    "/files/",
    "/mollie/list/",
    "/admin/banner",
    "/newsletter/",
    "/mailinglists/",
    "/tutors/",
]


@pytest.mark.parametrize("url", urls)
def test_admin_pages(url, admin_client, admin_user):
    admin_client.login(admin_user)
    rv = admin_client.get(url)
    assert rv.status_code == 200, rv.data
