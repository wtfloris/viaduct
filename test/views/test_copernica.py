import pytest

from celery.signals import task_sent
from app.models.mailinglist_model import MailingList
from app.models.user import User


@pytest.fixture
def mailinglists(db_session):
    mailinglist1 = MailingList(
        id=1,
        nl_name="Nieuwsbrief",
        en_name="Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    mailinglist2 = MailingList(
        id=2,
        nl_name="Bedrijfsinformatie",
        en_name="Company information",
        copernica_column_name="Bedrijfsinformatie",
        members_only=True,
    )

    db_session.add(mailinglist1)
    db_session.add(mailinglist2)
    db_session.commit()

    return [mailinglist1, mailinglist2]


@pytest.fixture
def user_newsletter(db_session, mailinglists, user_factory):
    user: User = user_factory()
    user.copernica_id = 12345
    user.mailinglists = [mailinglists[0]]

    db_session.commit()

    return user


def test_retrieve_webhook_verification_code(app, anonymous_client):
    filename = "copernica-123-456.txt"
    content = "1234567890qwertyuiop"

    app.config["COPERNICA_WEBHOOK_VERIFICATION_ENABLED"] = True
    app.config["COPERNICA_WEBHOOK_VERIFICATION_FILENAME"] = filename
    app.config["COPERNICA_WEBHOOK_VERIFICATION_CONTENT"] = content

    rv = anonymous_client.get("/" + filename)

    assert rv.status_code == 200
    assert rv.data.decode("utf-8") == content


def test_retrieve_webhook_verification_code_disabled(app, anonymous_client):
    filename = "copernica-123-456.txt"

    app.config["COPERNICA_WEBHOOK_VERIFICATION_ENABLED"] = False
    app.config["COPERNICA_WEBHOOK_VERIFICATION_FILENAME"] = filename
    app.config["COPERNICA_WEBHOOK_VERIFICATION_CONTENT"] = ""

    rv = anonymous_client.get("/" + filename)

    assert rv.status_code == 404


def test_call_webhook_correct_token(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    @task_sent.connect
    def assert_no_tasks(*args, **kwargs):
        assert kwargs["sender"] == "app.task.copernica.update_user"
        pytest.fail("webhook should not trigger Copernica sync task")

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "database": "1",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200
    assert (
        user_newsletter.mailinglists.count() == 1
        and user_newsletter.mailinglists[0] == mailinglists[1]
    )


def test_call_webhook_incorrect_token(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "database": "1",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=somethingelse", data=webhook_data
    )

    assert rv.status_code == 200

    # Assert that the user's preferences have not changed
    assert (
        user_newsletter.mailinglists.count() == 1
        and user_newsletter.mailinglists[0] == mailinglists[0]
    )


def test_call_webhook_incorrent_type(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "create",
        "profile": "54321",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_incorrent_database(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "321",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_incorrent_profile(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "update",
        "profile": "54321",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[NogEenMailinglijst]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_non_mailinglist_parameters(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "parameters[NogEenOnbekendeMailinglijst]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_missing_mailinglist_parameters(
    app, anonymous_client, user_newsletter, mailinglists
):
    app.config["COPERNICA_DATABASE_ID"] = "1"
    app.config["COPERNICA_WEBHOOK_TOKEN"] = "123abc"

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200
