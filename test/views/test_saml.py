import pytest
from flask_wtf.csrf import generate_csrf

from app.enums import ProgrammeType
from app.models.education import Education
from app.models.user import User
from app.service.saml_service import (
    ATTR_URN_GIVEN_NAME,
    ATTR_URN_MAIL,
    ATTR_URN_PERSON_AFFILIATION,
    ATTR_URN_PREFERRED_LANGUAGE,
    ATTR_URN_SURNAME,
    ATTR_URN_UID,
    SAML_DATA_ATTRIBUTES,
    SAML_DATA_IS_AUTHENTICATED,
    SESSION_SAML_DATA,
)
from conftest import CustomClient


@pytest.fixture
def education_msc_clsjd(db_session):
    e = Education()
    e.is_via_programme = True
    e.datanose_code = "MSc CLSJD"
    e.en_name = e.nl_name = "Master Computational Science (joint degree)"
    e.programme_type = ProgrammeType.MASTER
    db_session.add(e)
    db_session.commit()
    return e


def test_signup_saml_response(
    anonymous_client: CustomClient,
    db_session,
    user_factory,
    education_msc_clsjd,
    requests_mocker,
):
    existing_unconfirmed_user = user_factory()
    existing_unconfirmed_user.student_id = "123456780"
    existing_unconfirmed_user.student_id_confirmed = False
    db_session.commit()

    # TODO Find out why mypy says session_transaction does not return a value
    with anonymous_client.session_transaction() as session:  # type: ignore
        session[SESSION_SAML_DATA] = {
            SAML_DATA_IS_AUTHENTICATED: True,
            SAML_DATA_ATTRIBUTES: {
                ATTR_URN_UID: ["123456780"],
                ATTR_URN_PERSON_AFFILIATION: ["student"],
                ATTR_URN_GIVEN_NAME: ["John"],
                ATTR_URN_SURNAME: ["Doe"],
                ATTR_URN_MAIL: ["john.doe@student.uva.nl"],
                ATTR_URN_PREFERRED_LANGUAGE: ["en"],
            },
        }

    requests_mocker.get(
        "https://api.datanose.nl/Programmes/123456780",
        json=[
            {
                "Code": "MSc PhA",
                "Name": "Master Physics and Astronomy (joint degree)",
                "Type": "Master",
            },
            {
                "Code": "MSc CLSJD",
                "Name": "Master Computational Science (joint degree)",
                "Type": "Master",
            },
        ],
    )
    rv = anonymous_client.get("/sign-up/process-saml-response/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-up/process-saml-response/",
        data={
            "csrf_token": generate_csrf(),
            "birth_date": "2000-01-01",
            "study_start": "2013-01-01",
            "mailinglists": [],
            "agree_with_privacy_policy": True,
            "first_name": "John",
            "last_name": "Doe",
            "email": "john.doe@student.uva.nl",
            "address": "Street 10",
            "zip": "1111AA",
            "city": "Amsterdam",
            "country": "Nederland",
        },
    )
    assert rv.status_code == 200

    u = db_session.query(User).filter(User.student_id == "123456780").one()
    assert u.student_id_confirmed
    assert education_msc_clsjd in u.educations
    u = db_session.query(User).filter(User.id == existing_unconfirmed_user.id).one()
    assert u.student_id == ""
    assert not u.student_id_confirmed
