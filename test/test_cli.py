from flask_wtf.csrf import generate_csrf
from sqlalchemy import inspect
from app.models.user import User


def test_create_db(app, db, db_session, anonymous_client):
    runner = app.test_cli_runner()
    result = runner.invoke(args=["dropdb"])
    assert result.exit_code == 0

    with app.app_context():
        tables = inspect(db.engine).get_table_names()
        # The dropped tables are based on available metadata, alembic_version
        # is not in the metadata. However createdb may leave it from previous
        # runs.
        assert tables in ([], ["alembic_version"])

    create_input = [
        "john.doe@student.uva.nl",
        "password",
        "password",  # confirm
        "Maico",
        "Timmerman",
    ]

    result = runner.invoke(args=["createdb"], input="\n".join(create_input))
    assert result.exit_code == 0

    table_names = set(inspect(db.engine).get_table_names())
    # The "alembic_version" is not in the metadata.
    assert set(db.metadata.tables).issubset(table_names)

    user = db_session.query(User).one()
    assert user.first_name == "Maico"
    assert user.last_name == "Timmerman"

    rv = anonymous_client.get("/sign-in/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": user.email,
            # Raw password is only set in the pytest fixtures.
            "password": "password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200
