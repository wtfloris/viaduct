<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <xsl:for-each select="items/item">
            <font size="2"><b><xsl:value-of select="sdate"/>:</b><br/></font>
            <font size="2"><a href="#activities{id}" style="text-decoration:none; color:#ffffff;"><xsl:value-of select="nl/title"/></a></font><br /><br />
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
