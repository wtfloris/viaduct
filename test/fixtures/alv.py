import pytest

from typing import Dict


@pytest.fixture
def alv_minutes_data() -> Dict[str, str]:
    return {
        "main.tex": r"""
\documentclass{minutes}
\usepackage[utf8]{inputenc}
\usepackage[dutch]{babel}

% Vul hier de data in
\title{ALV titel}
\author{Notulist}
\date{01-01-1970}

% Hierin staan de namen van alle aanwezigen zodat je \naam kan typen
\input{names.tex}

\begin{document}
\chairman{Voorzitter Y\xspace}
\secretary{Notulist Z\xspace}
\starttime{HH:MM\xspace}
\stoptime{HH:MM\xspace}
\location{XY.WYV\xspace}

% Hierin staan alle aanwezigen op de ALV, eventueel met binnenkomst en tijd van weg gaan
\input{present.tex}

\maketitle

% Alle machtigen
\input{machtigingen.tex}

\begingroup
\linenumbers
\modulolinenumbers[5]

\section{Opening}

\timestamp{13:37}{X opent de vergadering}

\decision{Dit is een besluit}

\task{Commissie}{Doe dit actiepunt}
\task{Bestuur}{Doe dit actiepunt}

\pagebreak

De uitslag van de stemming is als volgt:

\begin{vote}{De verwoording van de stemming aldus de voorzitter}
Voor & 1 \\
Tegen & 3 \\
Blanco & 3 \\
Onthouden & 7 \\
\end{vote}

\decision{Het besluit wat uit de stemming is voortgekomen}

% Op deze manier worden de verschillende agendapunten geimporteerd
\input{agenda/01-mededelingen.tex}


\section{Sluiting}
\timestamp{13:38}{X sluit de vergadering}

% Hierdoor wordt de besluitenlijst automatisch onderaan gezet
\decisionlist
\tasklist

\end{document}

""",
        "names.tex": r"""
\newcommand{\wilco}{\name{Wilco Kruijer}}
\newcommand{\mick}{\name{Mick Vermeulen}}
% Heel belangrijk, \via dikgedrukt
\newcommand{\via}{\textbf{via}\xspace}

""",
        "present.tex": r"""
\present{
    \wilco,
    \mick (vanaf 17:44)
}
""",
        "machtigingen.tex": r"""
\section*{Machtigingen}
\wilco machtigt \mick; \\

""",
        "agenda/01-mededelingen.tex": r"""
\section{Mededelingen}
De machtigingen worden doorgenomen. \\
Blabla.
""",
    }
