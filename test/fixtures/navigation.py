import pytest

from app.service import navigation_service


@pytest.fixture
def sample_navigation(db, app, page_revision):
    page, revision = page_revision
    with app.test_request_context(headers=[("Accept-Language", "en")]):
        activities = navigation_service.create_entry(
            "activities",
            "Activiteiten",
            "Activities",
        )
        assert activities.href == "/activities/"
        page_entry = navigation_service.create_entry(
            "page", "Page", "Pagina", page_id=page.id
        )
        assert page_entry.href == "/path"
        news = navigation_service.create_entry(
            "url",
            "URL internal",
            "URL intern",
            url="/news/",
            order_children_alphabetically=True,
        )
        assert news.href == "/news"
        archive = navigation_service.create_entry(
            "url",
            "URL internal 2",
            "URL intern 2",
            parent_id=news.id,
            url="/activities/archive/",
            order_children_alphabetically=True,
        )
        assert archive.href == "/activities/archive"
        pretix = navigation_service.create_entry(
            "url",
            "URL extern pretix",
            "URL intern pretix",
            parent_id=news.id,
            url="pretix.svia.nl",
            external=True,
        )
        assert pretix.href == "https://pretix.svia.nl"
        pos = navigation_service.create_entry(
            "url",
            "URL extern pos",
            "URL intern pos",
            parent_id=news.id,
            url="pos.svia.nl",
            external=True,
        )
        assert pos.href == "https://pos.svia.nl"

        entries = [activities, page_entry, news, archive, pretix, pos]
        db.session.add_all(entries)
        db.session.commit()
    return entries
