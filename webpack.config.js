const path = require("path");

const webpack = require("webpack");
const { VueLoaderPlugin } = require("vue-loader");
const CssExtractPlugin = require("mini-css-extract-plugin");
const FriendlyErrorsWebpackPlugin = require("friendly-errors-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const CleanObsoleteChunks = require("webpack-clean-obsolete-chunks");
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

module.exports = function (env) {
    const plugins = [
        new VueLoaderPlugin(),
        new FriendlyErrorsWebpackPlugin(),
        new CssExtractPlugin({
            filename: "styles.css",
        }),
        new MomentLocalesPlugin({
            localesToKeep: ["en", "nl"],
        }),
    ];

    let tsTranspileOnly = false;
    let stats = "normal";
    if (env && env["production"]) {
        console.log("Building for production");
        plugins.push(
            new BundleAnalyzerPlugin({
                analyzerMode: "static",
                openAnalyzer: false,
            })
        );
    } else {
        plugins.push(
            new BundleAnalyzerPlugin({
                analyzerHost: "0.0.0.0",
                analyzerPort: 8891,
                openAnalyzer: false,
            })
        );

        // Fork TS plugin starts TypeScript type checker in a new thread, this
        // increases compile time for development. Do not use for production.
        plugins.push(
            new ForkTsCheckerWebpackPlugin({
                async: false,
                typescript: {
                    configFile: path.resolve(__dirname, "./tsconfig.json"),
                    extensions: {
                        vue: true,
                    },
                },
            })
        );

        // Disable type checking for ts-loader as it is done by above plugin instead
        tsTranspileOnly = true;
        // Disable webpack logging, as we use the FriendlyErrors plugin instead
        stats = "none";

        // In development remove all old chunks to not overflow developer filesystem.
        plugins.push(new CleanObsoleteChunks({}));

        // Source maps in develop
        plugins.push(new webpack.SourceMapDevToolPlugin({}));
    }

    return {
        stats: stats,
        entry: ["./frontend/index.ts", "./frontend/assets/scss/app.scss"],
        output: {
            filename: "main.js",
            chunkFilename: "chunk-[name]-[contenthash:8].js",
            path: path.resolve(__dirname, "./app/static/"),
            publicPath: "/static/",
        },
        optimization: {
            splitChunks: {
                maxAsyncRequests: 10,
                maxInitialRequests: 5,
            },
        },
        resolve: {
            extensions: [".js", ".json", ".ts", ".tsx", ".js", ".vue"],
            alias: {
                vue: "vue/dist/vue.common.js",
            },
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    include: path.resolve(__dirname, "./frontend"),
                    use: ["style-loader", "css-loader"],
                },
                {
                    test: /\.less$/,
                    include: path.resolve(__dirname, "./frontend"),
                    use: [
                        { loader: "style-loader" },
                        { loader: "css-loader" },
                        {
                            loader: "less-loader",
                            options: {
                                lessOptions: {
                                    // Ant design uses javascript in their less files.
                                    javascriptEnabled: true,
                                },
                            },
                        },
                    ],
                },
                {
                    test: /\.vue$/,
                    include: path.resolve(__dirname, "./frontend"),
                    loader: "vue-loader",
                    options: {
                        esModule: true,
                    },
                },
                {
                    resourceQuery: /blockType=i18n/,
                    type: "javascript/auto",
                    loader: "@kazupon/vue-i18n-loader",
                },
                {
                    test: /\.sass$/,
                    include: path.resolve(__dirname, "./frontend"),
                    use: [
                        "style-loader",
                        "css-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                sassOptions: {
                                    indentedSyntax: true,
                                },
                            },
                        },
                    ],
                },
                {
                    test: /\.scss$/,
                    include: path.resolve(__dirname, "./frontend"),
                    exclude: path.resolve(
                        __dirname,
                        "./frontend/assets/scss/app.scss"
                    ),
                    use: ["style-loader", "css-loader", "sass-loader"],
                },
                {
                    test: path.resolve(
                        __dirname,
                        "./frontend/assets/scss/app.scss"
                    ),
                    use: [CssExtractPlugin.loader, "css-loader", "sass-loader"],
                },
                {
                    test: /\.js$/,
                    include: path.resolve(__dirname, "./frontend"),
                    exclude: /node_modules\/vue-carousel/,
                    enforce: "pre",
                    loader: "source-map-loader",
                },
                {
                    test: /\.tsx?$/,
                    include: path.resolve(__dirname, "./frontend"),
                    loader: "ts-loader",
                    exclude: /node_modules/,
                    options: {
                        transpileOnly: tsTranspileOnly,
                        appendTsSuffixTo: [/\.vue$/],
                    },
                },
                {
                    test: /\.pug$/,
                    include: path.resolve(__dirname, "./frontend"),
                    loader: "pug-plain-loader",
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    include: path.resolve(__dirname, "./frontend"),
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]?[hash]",
                        outputPath: "images/",
                    },
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[ext]",
                                outputPath: "fonts/",
                            },
                        },
                    ],
                },
            ],
        },
        plugins: plugins,
        devtool: false,
    };
};
