import distutils.util
import logging
from typing import Callable, List

from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import scoped_session, sessionmaker

_logger = logging.getLogger(__name__)


class Config(object):
    post_load_callbacks: List[Callable[["Config"], None]] = []

    def __init__(self, database_url=None):
        """
        Initialize the config.

        Load the config values from the environment and do dynamic conversion.
        """

        # Database connection
        self.SECRET_KEY = "defaultsecretkeythatshouldbeoverwritten"

        # Flask-WTForms recaptcha.
        self.RECAPTCHA_PUBLIC_KEY = ""
        self.RECAPTCHA_PRIVATE_KEY = ""

        # Google apps synchronization.
        self.GOOGLE_CALENDAR_ID = ""

        # Git lab token for bug report.
        self.GITLAB_TOKEN = ""

        # Mollie payment provider config.
        self.MOLLIE_KEY = ""

        # Copernica e-mailing synchronization configuration.
        self.COPERNICA_ENABLED = False
        self.COPERNICA_API_KEY = ""
        self.COPERNICA_DATABASE_ID = ""
        self.COPERNICA_NEWSLETTER_TOKEN = ""
        self.COPERNICA_WEBHOOK_TOKEN = ""
        self.COPERNICA_WEBHOOK_VERIFICATION_ENABLED = False
        self.COPERNICA_WEBHOOK_VERIFICATION_FILENAME = ""
        self.COPERNICA_WEBHOOK_VERIFICATION_CONTENT = ""

        # Copernina document IDs can be found in
        self.COPERNICA_WELCOME_MAIL_NL_DOC_ID = 483
        self.COPERNICA_WELCOME_MAIL_EN_DOC_ID = 484

        # DOMJudge competition integration
        self.DOMJUDGE_ENABLED = False
        self.DOMJUDGE_ADMIN_USERNAME = ""
        self.DOMJUDGE_ADMIN_PASSWORD = ""
        self.DOMJUDGE_URL = ""
        self.DOMJUDGE_USER_PASSWORD = ""

        # Sentry error capturing
        self.SENTRY_DSN = ""
        self.SENTRY_DSN_FRONTEND = (
            "https://d20fbd1634454649bd8877942ebb5657" "@sentry.io/1285048"
        )
        self.ENVIRONMENT = "Development"
        self.DEVELOPER_MAIL = ""

        # Path for SAML config and certificates
        self.SAML_PATH = "saml/develop/"

        # Events calendar
        self.EVENTS_CALENDAR_ADD_CALENDAR_URL = (
            "https://calendar.google.com/calendar/render?"
            "cid=via.uvastudent.org_rdn1ffk47v0gmla0oni69egmhk%40"
            "group.calendar.google.com"
        )
        self.EVENTS_CALENDAR_ICS_URL = (
            "https://calendar.google.com/calendar/ical/"
            "via.uvastudent.org_rdn1ffk47v0gmla0oni69egmhk%40"
            "group.calendar.google.com/public/basic.ics"
        )

        # Flickr API
        self.FLICKR_USER = "dummy"
        self.FLICKR_API_KEY = "dummy"
        self.FLICKR_SECRET = "dummy"
        self.FLICKR_TOKEN = "dummy"
        self.FLICKR_TOKEN_SECRET = "dummy"

        # Pretix API
        self.PRETIX_ENABLED = False
        self.PRETIX_HOST = "http://pretix.svia.nl"
        self.PRETIX_TOKEN = ""
        self.PRETIX_ORGANIZER = "via"

        # OVslide config
        self.OV_BUS_LOCATION = "amsterdam/bushalte-university-college"
        self.OV_TRAIN_LOCATION = "station-amsterdam-science-park"

        # Membership
        self.MEMBERSHIP_NORMAL_PRICE = 2000
        self.MEMBERSHIP_MASTER_PRICE = 0

        if database_url:
            self.load_config(database_url=database_url)

        #
        # ================ ONLY NON-DB OPTIONS AFTER THIS ================
        #

        # These must be defined after `load_config`, since they are not
        # defined in the database.
        self.RECAPTCHA_OPTIONS = {"theme": "white"}

        self.SENTRY_USER_ATTRS = ["name", "email"]
        self.SENTRY_CONFIG = {"environment": self.ENVIRONMENT}

        self.CACHE_TYPE = "filesystem"
        self.CACHE_DIR = "cache"

        # Miscellaneous.
        self.DEBUG_TB_INTERCEPT_REDIRECTS = False
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False

        self.JSON_SORT_KEYS = False

        # Authlib
        self.OAUTH2_REFRESH_TOKEN_GENERATOR = True

        # Do not suggest when 404'ing in API's.
        self.ERROR_404_HELP = False

    def load_config(self, database_url):
        engine = create_engine(database_url)
        session = scoped_session(sessionmaker(bind=engine))

        if not inspect(engine).has_table("setting"):
            _logger.warning("Not loading database settings, table missing")
            return

        settings = {
            s[0]: s[1] for s in session.execute("SELECT key, value FROM setting")
        }

        for key in dir(self):
            if not key.isupper():
                continue

            if key not in settings:
                continue

            setting_type = getattr(self, key)
            setting_value = settings[key]
            if isinstance(setting_type, bool):
                setting_value = distutils.util.strtobool(setting_value)

            setattr(self, key, setting_value)
            _logger.info(f"{key} = {setting_value}")

        for callback in self.post_load_callbacks:
            _logger.info("Calling config callback: %s", callback)
            callback(self)

    @classmethod
    def post_load(cls, f):
        cls.post_load_callbacks.append(f)
        return f
